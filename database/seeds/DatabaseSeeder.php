<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Year;
use App\Holiday;
use App\RoomType;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        if (User::where('role', 'admin')->count() == 0) {
            $admin = new User();
            $admin->name = 'Administrator';
            $admin->email = 'admin@dario.it';
            $admin->password = Hash::make('admin');
            $admin->role = 'admin';
            $admin->save();
        }

        if (RoomType::all()->count() == 0) {
            $types = ['Corsi', 'Feste', 'Consulenze', 'Riunioni'];
            foreach($types as $type) {
                $t = new RoomType();
                $t->name = $type;
                $t->save();
            }
        }

        $current_month = date('m');
        if ($current_month < 8) {
            $first_year = date('Y', strtotime('-1 years'));
            $end_year = date('Y');
        }
        else {
            $first_year = date('Y');
            $end_year = date('Y', strtotime('+1 years'));
        }

        $year_name = sprintf('%s/%s', $first_year, $end_year);

        if (Year::where('name', $year_name)->count() == 0) {
            $year = new Year();
            $year->name = $year_name;
            $year->start = date('Y-m-d', strtotime('Sep 1, ' . $first_year));
            $year->end = date('Y-m-d', strtotime('Jul 31, ' . $end_year));
            $year->save();

            $easter = date('Y-m-d', easter_date($end_year));
            $holidays = [
                $first_year . '-11-01' => 'Ognissanti',
                $first_year . '-12-08' => 'Immacolata Concezione',
                $first_year . '-12-25' => 'Natale',
                $first_year . '-12-26' => 'Santo Stefano',
                $end_year . '-01-01' => 'Capodanno',
                $end_year . '-01-06' => 'Epifania',
                $easter => 'Pasqua',
                date('Y-m-d', strtotime($easter . ' +1 days')) => 'Pasquetta',
                $end_year . '-04-25' => 'Liberazione',
                $end_year . '-05-01' => 'Festa del Lavoro',
                $end_year . '-06-02' => 'Festa della Repubblica',
            ];

            foreach($holidays as $date => $name) {
                $h = new Holiday();
                $h->year = $year->id;
                $h->name = $name;
                $h->date = $date;
                $h->save();
            }
        }
    }
}
