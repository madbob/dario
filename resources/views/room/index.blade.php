@extends('layouts.app')

@section('content')
    @include('layouts.menu', ['active' => 'rooms'])

    <div class="ui fluid container">
        <div class="ui grid fullwidth">
            <div class="sixteen wide column">
                <form method="POST" action="{{ route('room.update', 0) }}" class="ui form">
                    @csrf
                    @method('PUT')

                    <table id="rooms" class="ui basic table dynamic-table">
                        <thead>
                            <tr>
                                <th width="13%">Nome</th>
                                <th width="19%">Descrizione</th>
                                <th width="19%">Note (aggiunte ai documenti delle assegnazioni)</th>
                                <th width="12%">Tipi</th>
                                <th width="12%">Attrezzature</th>
                                <th width="10%">Capacità</th>
                                <th width="10%">Prezzo</th>
                                <th width="5%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($rooms as $room)
                                @include('room.edit', ['room' => $room])
                            @endforeach

                            @include('room.edit', ['room' => null])
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">
                                    <button class="ui orange basic button add-row">Aggiungi Sala</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                    <div class="ui divider"></div>

                    <button class="ui positive button" type="submit">Salva</button>
                </form>
            </div>
        </div>
    </div>
@endsection
