<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PDF;

use App\Room;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    private function readRequest(Request $request)
    {
        $user = $request->user();

        $rooms = $user->rooms;
        $grid_columns = ((myConf('end_hour') - myConf('start_hour')) * 2) + 1;

        $room_id = $request->input('room', '0');
        $room = Room::find($room_id);

        if ($room || $room_id == 999) {
            if ($request->has('date')) {
                $is_monday = (date('N', strtotime($request->input('date'))) == 1);
                if ($is_monday)
                    $start = new \DateTime(date('Y-m-d', strtotime($request->input('date'))));
                else
                    $start = new \DateTime(date('Y-m-d', strtotime('previous monday', strtotime($request->input('date')))));

                $end = new \DateTime(date('Y-m-d', strtotime('next monday', strtotime($request->input('date')))));
            }
            else {
                $is_monday = (date('N') == 1);
                if ($is_monday)
                    $start = new \DateTime(date('Y-m-d 00:00:00'));
                else
                    $start = new \DateTime('last monday');

                $end = new \DateTime('next monday');
            }

            $days = new \DatePeriod($start, new \DateInterval('P1D'), $end);

            if ($room) {
                return ['room', compact('rooms', 'room', 'days', 'grid_columns')];
            }
            else {
                return ['weekly', compact('rooms', 'room', 'days', 'grid_columns')];
            }
        }
        else if ($room_id == 0) {
            if ($request->has('date')) {
                $day = new \DateTime($request->input('date'));
            }
            else {
                $day = new \DateTime(date('Y-m-d 00:00:00'));
            }

            return ['day', compact('rooms', 'room', 'day', 'grid_columns')];
        }
    }

    public function index(Request $request)
    {
        list($template, $params) = $this->readRequest($request);
        return view($template, $params);
    }
}
