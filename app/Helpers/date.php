<?php

function months()
{
    return [
        'Gennaio',
        'Febbraio',
        'Marzo',
        'Aprile',
        'Maggio',
        'Giugno',
        'Luglio',
        'Agosto',
        'Settembre',
        'Ottobre',
        'Novembre',
        'Dicembre'
    ];
}

function days()
{
    return [
        'Domenica',
        'Lunedì',
        'Martedì',
        'Mercoledì',
        'Giovedì',
        'Venerdì',
        'Sabato'
    ];
}

function readDate($date)
{
    if (empty($date))
        return '';

    if (strpos($date, '/') !== false) {
        list($day, $month, $year) = explode('/', $date);
        return sprintf('%s-%s-%s', $year, $month, $day);
    }
    else {
        return $date;
    }
}

function writeDate($date)
{
    if (is_null($date)) {
        return '';
    }
    else if (is_string($date)) {
        list($year, $month, $day) = explode('-', $date);
        return sprintf('%s/%s/%s', $day, $month, $year);
    }
    else {
        return $date->format('d/m/Y');
    }
}

function widgetDate($date)
{
    if (is_string($date)) {
        list($year, $month, $day) = explode('-', $date);
        return sprintf('%s-%s-%s', $year, $month, $day);
    }
    else {
        return $date->format('Y-m-d');
    }
}

function fullDate($date, $year = true)
{
    if (is_string($date))
        $date = new \DateTime($date);

    if ($year)
        $format = '%a %d/%m/%Y';
    else
        $format = '%a %d/%m';

    return ucwords(Carbon::instance($date)->formatLocalized($format));
}

function floatToTime($time)
{
    if (is_numeric($time)) {
        if (strpos($time, '.') !== false)
            return sprintf('%02d:30', intval($time));
        else
            return sprintf('%02d:00', $time);
    }
    else {
        return reducedTime($time);
    }
}

function timeToFloat($time)
{
    if (is_numeric($time)) {
        return $time;
    }
    else {
        $tokens = explode(':', $time);
        if (count($tokens) == 3)
            list($hours, $minutes, $seconds) = $tokens;
        else
            list($hours, $minutes) = $tokens;

        if ($minutes == '30')
            return ((int) $hours) + 0.5;
        else
            return (int) $hours;
    }
}

function reducedTime($time)
{
    if (is_numeric($time)) {
        return floatToTime();
    }
    else {
        $tokens = explode(':', $time);
        if (count($tokens) == 3) {
            list($hours, $minutes, $seconds) = $tokens;
            return sprintf('%02d:%02d', $hours, $minutes);
        }
        else {
            return $time;
        }
    }
}
