<div>
    <input type="hidden" name="customer_id" value="new">
    <input type="hidden" name="grant_id" value="new">
    @include('customer.registry', ['customer' => null, 'prefix' => 'customer_'])

    <br>

    <table id="contacts" class="ui basic table dynamic-table">
        <thead>
            <tr>
                <th width="90%">Contatti (seleziona quello principale per l'assegnazione!)</th>
                <th width="5%">&nbsp;</th>
                <th width="5%">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <tr class="reference-row">
                <td>
                    <div class="field ui search autocompletable" data-autocomplete-url="/contact/search/?search={query}" data-autocomplete-target="parent_tr">
                        <input type="hidden" name="customer_contact_id[]" data-autocompletable-map="id">
                        <div class="ui icon input">
                            <input type="text" name="name" class="prompt" placeholder="Cerca tra i contatti esistenti">
                        </div>
                    </div>
                </td>
                <td>
                    <div class="ui toggle checkbox explicit">
                        <input type="radio" name="contact_id" tabindex="0" class="hidden" value="" data-autocompletable-map="id">
                    </div>
                </td>
                <td>
                    <button class="ui red icon button pull-right remove-row"><i class="trash icon"></i></button>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <button class="ui orange basic button add-row" data-table-target="#contacts">Aggiungi Contatto</button>
                </td>
            </tr>
        </tfoot>
    </table>

    <hr>

    <br>

    @include('grant.embed', ['grant' => null, 'customer' => null])
</div>
