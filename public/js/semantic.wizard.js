(function ($) {
    $.fn.extend({
        _reviewWizardButtons: function() {
            var index = $(this).find('.steps .step.active').index();
            var actions = $(this).find('.wizard-actions');

            if (index == 0) {
                actions.find('.wizard-prev').prop('disabled', true);
                actions.find('.wizard-next').show();
                actions.find('.wizard-finish').hide();
            }
            else {
                actions.find('.wizard-prev').prop('disabled', false);

                if (index == $(this).find('.steps .step').length - 1) {
                    actions.find('.wizard-next').hide();
                    actions.find('.wizard-finish').show();
                }
                else {
                    actions.find('.wizard-next').show();
                    actions.find('.wizard-finish').hide();
                }
            }
        },

        _moveWizard: function(offset) {
            var index = $(this).find('.steps .step.active').index();
            var new_index = index + offset;

            if (new_index >= 0 && new_index < $(this).find('.steps .step').length) {
                var test = $(this).triggerHandler('wizard-page-changing', index, new_index);
                if (test) {
                    $(this).find('.steps .step').removeClass('active').eq(new_index).addClass('active');
                    $(this).find('.wizard-step').removeClass('active').hide().eq(new_index).addClass('active').show();
                }
            }

            $(this)._reviewWizardButtons();
        }
    });

    $.fn.wizard = function() {
        $(this).each(function() {
            var this_wizard = $(this);
            this_wizard.find('.wizard-step:not(.active)').hide();

            this_wizard._reviewWizardButtons();

            this_wizard.find('.wizard-actions .wizard-prev').click(function(e) {
                e.preventDefault();
                this_wizard._moveWizard(-1);
            });

            this_wizard.find('.wizard-actions .wizard-next').click(function(e) {
                e.preventDefault();
                this_wizard._moveWizard(1);
            });
        });

        return $(this);
    };
}(jQuery));
