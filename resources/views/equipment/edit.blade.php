<tr class="{{ $equipment ? '' : 'reference-row' }}">
    <td>
        <input type="hidden" name="id[]" value="{{ $equipment ? $equipment->id : 'empty' }}">
        <div class="field">
            <input type="text" name="name[]" value="{{ $equipment ? $equipment->name : '' }}">
        </div>
    </td>
    <td>
        <button class="ui red icon button pull-right remove-row"><i class="trash icon"></i></button>
    </td>
</tr>
