<div class="ui large modal">
    <i class="close icon"></i>
    <div class="header">
        Modifica Prenotazione
    </div>
    <div class="scrolling content">
        <form method="POST" action="{{ route('booking.update', $booking->id) }}" class="ui form">
            @csrf
            @method('PUT')

            <input type="hidden" name="booking_id" value="{{ $booking->id }}">
            <input type="hidden" name="selected_room" value="{{ $booking->room_id }}">
            @foreach($booking->equipments as $eq)
                <input type="hidden" name="selected_equipment[]" value="{{ $eq->id }}">
            @endforeach
            <input type="hidden" name="selected_day" value="{{ $booking->start }}">
            <input type="hidden" name="selected_start_hour" value="{{ floatToTime($booking->recursion_obj->days[0]->from) }}">
            <input type="hidden" name="selected_end_hour" value="{{ floatToTime($booking->recursion_obj->days[0]->to) }}">

            <div class="ui top attached tabular menu booking-wizard">
                <div class="item active" data-tab="customers">Chi</div>
                <div class="item" data-tab="spaces">Quando</div>
                <div class="item" data-tab="payments">Quanto</div>
            </div>

            <div class="ui bottom attached tab segment active" data-tab="customers">
                <div class="field ui search customer_complete">
                    <label>Cliente</label>
                    <p>{{ $booking->grant->customer->name }}</p>
                </div>

                @include('grant.embed_ro', ['grant' => $booking->grant])
            </div>

            <div class="ui bottom attached tab segment" data-tab="spaces">
                <div class="three fields">
                    <div class="field">
                        <label>Descrizione</label>
                        <input type="text" name="description" value="{{ $booking->description }}">
                    </div>

                    <div class="field">
                        <label>Colore</label>
                        <input type="text" class="color" name="color" value="{{ $booking->color }}">
                    </div>

                    <div class="required field">
                        <label>Tipo</label>
                        <div class="inline fields radio-loader" data-load-url="{{ route('booking.wizard', ['page' => 'spaces']) }}" data-load-area=".booking_spaces">
                            <div class="field">
                                <div class="ui radio checkbox">
                                    <input type="radio" name="type" value="recurring" class="hidden" required {{ $booking->type == 'recurring' ? 'checked' : '' }}>
                                    <label>Ricorrente</label>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui radio checkbox">
                                    <input type="radio" name="type" value="occasional" class="hidden" required {{ $booking->type == 'occasional' ? 'checked' : '' }}>
                                    <label>Occasionale</label>
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui radio checkbox">
                                    <input type="radio" name="type" value="manual" class="hidden" required {{ $booking->type == 'manual' ? 'checked' : '' }}>
                                    <label>Manuale</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>

                <div class="booking_spaces">
                    @if($booking->type == 'occasional')
                        @include('booking.spaces.single', [
                            'selected_room' => $booking->room_id,
                            'selected_equipment' => $booking->equipments->pluck('id')->toArray(),
                            'selected_day' => $booking->start,
                            'selected_end_day' => $booking->end,
                            'selected_start_hour' => floatToTime($booking->recursion_obj->days[0]->from),
                            'selected_end_hour' => floatToTime($booking->recursion_obj->days[0]->to)
                        ])
                    @elseif($booking->type == 'recurring')
                        @include('booking.spaces.recurring', [
                            'selected_room' => $booking->room_id,
                            'selected_equipment' => $booking->equipments->pluck('id')->toArray(),
                            'selected_day' => $booking->start,
                            'details' => $booking->recursion_obj
                        ])
                    @elseif($booking->type == 'manual')
                        @include('booking.spaces.manual', [
                            'selected_room' => $booking->room_id,
                            'selected_equipment' => $booking->equipments->pluck('id')->toArray(),
                            'details' => $booking->recursion_obj
                        ])
                    @endif
                </div>
            </div>

            <div class="ui bottom attached tab segment payments-wrapper" data-tab="payments">
                <div class="field">
                    <label>Numero Partecipanti</label>
                    <input type="number" name="partecipants" value="{{ $booking->partecipants }}">
                </div>

                <div class="field">
                    <label>Pagamento</label>
                    <div class="grouped fields radio-loader" data-load-url="{{ route('booking.wizard', ['page' => 'payments']) }}" data-load-area=".booking_payments">
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" name="payment" value="pay" class="hidden" required {{ $booking->payment == 'pay' ? 'checked' : '' }}>
                                <label>A Pagamento</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" name="payment" value="free" class="hidden" required {{ $booking->payment == 'free' ? 'checked' : '' }}>
                                <label>Gratuito</label>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui radio checkbox">
                                <input type="radio" name="payment" value="contribute" class="hidden" required {{ $booking->payment == 'contribute' ? 'checked' : '' }}>
                                <label>Contributo</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="booking_payments">
                    <?php $involved_days = $booking->getDays(false) ?>
                    @if($booking->payment == 'pay')
                        @include('booking.payments.full', [
                            'days' => $involved_days,
                            'unit_price' => $booking->unit_price,
                            'total_price' => $booking->total_price,
                            'payments' => $booking->payments,
                        ])
                    @elseif($booking->payment == 'free')
                        @include('booking.payments.free')
                    @elseif($booking->payment == 'contribute')
                        @include('booking.payments.single', [
                            'days' => $involved_days,
                            'unit_price' => $booking->unit_price,
                            'total_price' => $booking->total_price,
                            'payment' => $booking->payments->first(),
                        ])
                    @endif
                </div>

                @include('booking.payments.deposit', ['booking' => $booking])
            </div>

            <button class="ui positive button" type="submit">Salva</button>
            <a class="ui negative button right floated async-modal" href="#" data-modal-url="{{ route('booking.askdestroy', $booking->id) }}">Elimina o Annulla</a>
        </form>
    </div>
</div>
