<?php

function globalMultiInstallation()
{
    return true;
}

function instanceName()
{
    return substr($_SERVER['HTTP_HOST'], 0, strpos($_SERVER['HTTP_HOST'], '.'));
}

function localEnvFile()
{
    if (globalMultiInstallation() && isset($_SERVER['HTTP_HOST'])) {
        $instance = instanceName();
        return ('.env.' . $instance);
    }
    else {
        return '.env';
    }
}

function myConf($name)
{
    static $config_cache = [];

    if (!isset($config_cache[$name])) {
        $c = App\Config::where('name', $name)->first();
        if ($c == null)
            $c = App\Config::provideDefault($name);

        $config_cache[$name] = $c->value;
    }

    return $config_cache[$name];
}

function currentSocialYear()
{
    $year = null;
    $current_month = date('m');
    if ($current_month >= 8) {
        $test = sprintf('%s/%s', date('Y'), date('Y', strtotime('+1 years')));
        $year = App\Year::where('name', $test)->first();
    }

    if (is_null($year)) {
        $test = sprintf('%s/%s', date('Y', strtotime('-1 years')), date('Y'));
        $year = App\Year::where('name', $test)->first();
    }

    return $year;
}
