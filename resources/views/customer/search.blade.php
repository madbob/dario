<div>
    <div class="field ui search customer_complete">
        <div class="ui action input">
            <input class="prompt" type="text" name="customer_name" required>
            <input type="hidden" name="customer_id" value="">

            @if(isset($can_create_on))
                <button class="ui positive button async-portion" data-portion-url="{{ route('booking.wizard', 'new_customer') }}" data-container-id="{{ $can_create_on }}">Crea Nuovo Cliente</button>
            @endif
        </div>
        <div class="results"></div>
    </div>
</div>
