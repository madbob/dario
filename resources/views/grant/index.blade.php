@extends('layouts.app')

@section('content')
    @include('layouts.menu', ['active' => 'grants'])

    <div class="ui fluid container">
        <div class="ui grid fullwidth">
            <div class="sixteen wide column ui form">
                <a class="ui basic orange button async-modal" data-modal-url="{{ route('grant.create') }}">Aggiungi Assegnazione</a>

                @foreach(App\Year::orderBy('name', 'desc')->get() as $year)
                    <a class="ui orange {{ $current_year != $year->id ? 'basic' : '' }} button" href="{{ route('grant.index', ['year' => $year->id]) }}">{{ $year->name }}</a>
                @endforeach
            </div>

            <div class="sixteen wide column ui form">
                <div class="ui field">
                    <div class="ui action input">
                        <input type="text" placeholder="Filtra" class="table_filter" data-filter-target="#grants">
                        <button class="ui red button table_filter_button" value="payment_expired" data-filter-target="#grants">Pagamenti Scaduti</button>
                        <button class="ui green button table_filter_button" value="" data-filter-target="#grants">Tutti</button>
                    </div>
                </div>
            </div>

            <div class="sixteen wide column">
                <table id="grants" class="ui basic table dynamic-table" data-accepted-resource="year::{{ $current_year }}">
                    <thead>
                        <th width="20%">Cliente</th>
                        <th width="5%">Tipo</th>
                        <th width="5%">Protocollo</th>
                        <th width="20%">Descrizione</th>
                        <th width="20%">Prenotazioni</th>
                        <th width="20%">Pagamenti</th>
                        <th width="10%">&nbsp;</th>
                    </thead>
                    <tbody>
                        @foreach($grants as $grant)
                            <tr>
                                <td>
                                    <input type="hidden" name="grant_id[]" value="{{ $grant->id }}" class="id_into_table">
                                    <span data-autocompletable-map="customer_name">{{ $grant->customer->name }}</span>
                                </td>
                                <td data-autocompletable-map="types">{{ $grant->human_type }}</td>
                                <td data-autocompletable-map="number">{{ $grant->number }}</td>
                                <td data-autocompletable-map="description">{{ $grant->description }}</td>
                                <td colspan="2">
                                    <table class="extended">
                                        @foreach($grant->bookings as $booking)
                                            <tr>
                                                <td width="50%">
                                                    {!! $booking->description !!}
                                                </td>
                                                <td width="50%">
                                                    <ul>
                                                        @foreach($booking->payments as $payment)
                                                            <li>{!! $payment->printable_full_description !!}</li>
                                                        @endforeach
                                                    </ul>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>

                                    @if($grant->payments->where('payed', false)->where('date', '<', date('Y-m-d'))->count() != 0)
                                        <span class="hidden">payment_expired</span>
                                    @endif
                                </td>
                                <td>
                                    <span class="pull-right">
                                        <button data-autocompletable-map="edit_url" class="ui blue icon button async-modal" data-modal-url="{{ route('grant.edit', $grant->id) }}"><i class="zoom in icon"></i></button>
                                        <button data-autocompletable-map="destroy_url" class="ui red icon button async-modal" data-modal-url="{{ route('grant.askdestroy', $grant->id) }}"><i class="trash icon"></i></button>
                                    </span>
                                </td>
                            </tr>
                        @endforeach

                        <tr class="reference-row">
                            <td>
                                <input type="hidden" name="grant_id[]" value="" class="id_into_table">
                                <span data-autocompletable-map="customer_name"></span>
                            </td>
                            <td data-autocompletable-map="types"></td>
                            <td data-autocompletable-map="number"></td>
                            <td data-autocompletable-map="description"></td>
                            <td colspan="2">
                                <table class="extended">
                                </table>
                            </td>
                            <td>
                                <span class="pull-right">
                                    <button data-autocompletable-map="edit_url" class="ui blue icon button async-modal" data-modal-url=""><i class="zoom in icon"></i></button>
                                    <button data-autocompletable-map="destroy_url" class="ui red icon button async-modal" data-modal-url=""><i class="trash icon"></i></button>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
