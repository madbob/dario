@extends('layouts.app')

@section('content')
    @include('layouts.menu', ['active' => 'home', 'mode' => 'room'])

    @if($rooms->count() == 0)
        <h2 class="ui center aligned icon header">
            <i class="circular settings icon"></i>
            Per iniziare, crea almeno una sala.
        </h2>
    @else
        <div class="print-only">
            <h2>{{ $room->name }}</h2>
        </div>

        <div class="freeze-table">
            <table class="ui selectable structured table fixed celled main-grid fixed-header">
                <thead>
                    @include('layouts.weekly_header', ['days' => $days, 'column_select' => true])
                </thead>
                <tbody>
                    @include('booking.gridroom', ['days' => $days, 'room' => $room])
                </tbody>
            </table>
        </div>
    @endif
@endsection
