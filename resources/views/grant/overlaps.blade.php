<?php $overlaps = $grant->testOverlaps() ?>

@if($overlaps->isEmpty() === false)
    <div class="ui negative message">
        <p>
            Attenzione! Le prenotazioni in questa assegnazione hanno delle sovrapposizioni con altre prenotazioni!
        </p>
        <ul>
            @foreach($overlaps as $o)
                <li>{{ $o->booking->printable_description }} - {{ fullDate($o->date) }}</li>
            @endforeach
        </ul>
    </div>
@endif
