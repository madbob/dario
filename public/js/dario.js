function verifyEndDate(widget, date) {
    var end_widget = $(widget.attr('data-end-widget'));

    var check = end_widget.find('input:radio');
    if (check.length != 0 && check.prop('checked') == false)
        return;

    var end_date = end_widget.calendar('get focusDate');
    if (date > end_date)
        end_widget.calendar('set date', date);
}

(function ($) {
    $.fn.initWidgets = function() {
        $('.ui.checkbox', this).checkbox();
        $('select.dropdown', this).dropdown();
        $('.ui.dropdown', this).dropdown();

        $('.modal').draggable({
            handle: '.header'
        });

        $('.ui.accordion').accordion({
            onOpen: function() {
                var panel = $(this);
                panel.closest('.accordion').find('> .content').empty();

                $.ajax({
                    url: panel.attr('data-remote-url'),
                    method: 'GET',
                    success: function(data) {
                        var d = $(data);
                        panel.append(d);
                        d.initWidgets();
                    }
                });
            }
        });

        $('.tabular.menu .item', this).tab();
        $('input.color', this).colors();

        $('.ui.calendar:not(.navigation)', this).calendar({
            type: 'date',
            text: italianCalendar(),
            firstDayOfWeek: 1,
            formatter: {
                date: function(date, settings) {
                    return date.getDate() + '/' + (date.getMonth() + 1) + '/' + (date.getYear() + 1900);
                }
            },
            /*
                All'interno del div.ui.calendar ci possono essere diversi input
                "di servizio" (hidden o radio), questo è per forzare la
                selezione della sola casella di testo con la data. Senza questo,
                capita che - in virtù degli eventi triggerati al click - il
                popup del calendario si chiuda prima di poter selezionare un
                giorno
            */
            selector: {
                input: 'input:text'
            },
            popupOptions: {
                closable: false
            },
            onChange: function(date, text, mode) {
                var func = $(this).attr('data-change-function');
                if (func != null)
                    window[func]($(this), date);

                var input = $(this).find('input');
                setTimeout(function() {
                    input.change();
                }, 200);

                return true;
            }
        }).on('keydown', function(e) {
            /*
                Questo è per inibire l'edit diretto delle date senza passare dal
                widget del calendario
            */
            e.preventDefault();
        });

        $('.wizard', this).wizard().on('wizard-page-changing', function(old_index, new_index) {
            var ret = true;

            $(this).find('.wizard-step').eq(new_index).find('[required], [data-required]').each(function() {
                var data = $(this).val();
                if (data.length <= 0 && $(this).is(':visible')) {
                    $(this).closest('.field').addClass('error');
                    ret = false;
                }
            });

            return ret;
        });

        $('.autocompletable', this).each(function() {
            var input = $(this);
            var target = input.attr('data-autocomplete-target');
            var row = null;

            if (target == 'myself')
                row = input;
            else if (target == 'parent_tr')
                row = input.closest('tr');
            else
                row = $(target);

            $(this).search({
                apiSettings: {
                    url: input.attr('data-autocomplete-url')
                },
                onSelect: function(result, response) {
                    autocompleteData(row, result);
                }
            });

            $(this).on('click', '.undo', function() {
                autocompleteData(row, null);
            });
        });

        $('.customer_complete', this).search({
            apiSettings: {
                url: '/customer/search/?search={query}'
            },
            onSelect: function(result, response) {
                $(this).find('input[name=customer_id]').val(result.id);

                var area_grant = $(this).closest('.modal').find('.customer_grant');
                if (area_grant.length) {
                    $.ajax({
                        method: 'GET',
                        url: '/booking/wizard/customers',
                        data: $(this).closest('form').serialize(),
                        dataType: 'HTML',
                        success: function(data) {
                            var d = $(data);
                            area_grant.empty().append(d);
                            d.initWidgets();
                        }
                    });
                }

                var area_contacts = $(this).closest('form').find('.customer_contacts');
                if (area_contacts.length) {
                    $.ajax({
                        method: 'GET',
                        url: '/customer/contacts',
                        data: {
                            customer_id: result.id
                        },
                        dataType: 'HTML',
                        success: function(data) {
                            var d = $(data);
                            area_contacts.empty().append(d);
                            d.initWidgets();
                        }
                    });
                }
            }
        });

        $('.stackable_modal', this).each(function() {
            $(this).modal({
                allowMultiple: true,
                detachable: false
            });

            $(this).modal('attach events', $(this).attr('data-trigger-selector'));
        });
    };
}(jQuery));

/*
    https://css-tricks.com/snippets/jquery/make-jquery-contains-case-insensitive/
*/
$.expr[":"].contains_ci = $.expr.createPseudo(function(arg) {
    return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});

function italianCalendar()
{
    return {
        days: ['D', 'L', 'M', 'M', 'G', 'V', 'S'],
        months: ['Gennaio', 'Febbraio', 'Marzo', 'Aprile', 'Maggio', 'Giugno', 'Luglio', 'Agosto', 'Settembre', 'Ottobre', 'Novembre', 'Dicembre'],
        monthsShort: ['Gen', 'Feb', 'Mar', 'Apr', 'Mag', 'Giu', 'Lug', 'Ago', 'Set', 'Ott', 'Nov', 'Dic'],
        today: 'Oggi',
        now: 'Adesso',
        am: 'AM',
        pm: 'PM'
    };
}

function injectRemoteModal(url, data)
{
    $.ajax({
        url: url,
        method: 'GET',
        data: data,
        dataType: 'HTML',
        success: function(data) {
            $(data).appendTo('body').modal({
                onHidden: function() {
                    $(this).remove();
                }
            }).modal('show').initWidgets();
        }
    });
}

function autocompleteData(row, response)
{
    if (response != null) {
        for (var property in response) {
            if (response.hasOwnProperty(property)) {
                row.find('[data-autocompletable-map=' + property + ']').each(function() {
                    if ($(this).is('input')) {
                        $(this).val(response[property]);
                    }
                    else if ($(this).is('ul')) {
                        $(this).empty();
                        var elements = response[property];
                        for (var i = 0; i < elements.length; i++)
                            $(this).append('<li>' + elements[i].name + '</li>');
                    }
                    else {
                        if ($(this).hasClass('async-modal'))
                            $(this).attr('data-modal-url', response[property]);
                        else
                            $(this).text(response[property]);
                    }
                });
            }
        }
    }
    else {
        row.find('[data-autocompletable-map]').each(function() {
            if ($(this).is('input')) {
                $(this).val('');
            }
            else {
                if ($(this).hasClass('async-modal'))
                    $(this).attr('data-modal-url', '');
                else
                    $(this).text('');
            }
        });
    }
}

function adjustDynamicTable(table, data)
{
    var test = table.attr('data-accepted-resource');
    if (test != null) {
        test = test.split('::');
        if (data[test[0]] != test[1])
            return;
    }

    var row = null;
    var new_row = false;

    table.find('.id_into_table').each(function() {
        if ($(this).val() == data.id) {
            row = $(this).closest('tr');
            return false;
        }
    });

    if (row == null) {
        row = table.find('.reference-row').clone().removeClass('reference-row').appendTo($('tbody', table));

        var maxrows = parseInt(table.attr('data-max-rows'));
        if (maxrows != 0) {
            if (table.find('tbody tr:not(.reference-row)').length >= maxrows) {
                table.find('tfoot').hide();
            }
        }

        new_row = true;
    }

    autocompleteData(row, data);

    table.trigger(new_row ? 'added' : 'changed', data);
}

/*
    Usato come parametro "after_success" per il form di creazione di una nuova
    assegnazione
*/
function createBooking(data) {
    injectRemoteModal('/booking/create', {
        'grant_id': data.id
    });
}

$(document).ready(function() {
    /*
        Callbacks globali
    */

    $('#day-navigator').calendar({
        type: 'date',
        text: italianCalendar(),
        today: true,
        firstDayOfWeek: 1,
        initialDate: new Date($('input:hidden[name=current_selected_day]').val()),
        onChange: function(date, text, mode) {
            var room_id = $('input:hidden[name=current_selected_room]').val();
            window.location.replace('/?room=' + room_id + '&date=' + (date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()));
        },
    });

    $('#room-navigator a').click(function(e) {
        e.preventDefault();
        var room_id = $(this).attr('data-room-id');
        var date = $('#day-navigator').calendar('get focusDate');
        window.location.replace('/?room=' + room_id + '&date=' + (date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()));
    });

    $('#navigator-prev').click(function() {
        var room_id = $('input:hidden[name=current_selected_room]').val();
        var date = $('#day-navigator').calendar('get focusDate');
        if (room_id != 0)
            date.setDate(date.getDate() - 7);
        else
            date.setDate(date.getDate() - 1);

        window.location.replace('/?room=' + room_id + '&date=' + (date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()));
    });

    $('#navigator-next').click(function() {
        var room_id = $('input:hidden[name=current_selected_room]').val();
        var date = $('#day-navigator').calendar('get focusDate');
        if (room_id != 0)
            date.setDate(date.getDate() + 7);
        else
            date.setDate(date.getDate() + 1);

        window.location.replace('/?room=' + room_id + '&date=' + (date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate()));
    });

    $('#room-column-selector').columner({
        cookie_name: 'room-selection'
    }).on('columns-updated', function() {
        $('.freeze-table').freezeTable('update');
    });

    $('#day-column-selector').columner({
        cookie_name: 'day-selection'
    }).on('columns-updated', function() {
        $('.freeze-table').freezeTable('update');
    });

    $('.freeze-table').freezeTable({
        fixedNavbar: '.fixed.menu',
        freezeColumn: false,
        callback: function() {
            $('.clone-head-table-wrap').find('th:nth-child(1)').empty();
        }
    });

    $('body').on('click', '.print-page', function() {
        window.print();
    });

    $('.main-grid').on('click', 'td', function(e) {
        if ($(this).hasClass('occupied')) {
            var url = $(this).attr('data-slot-url');
            injectRemoteModal(url, null);
        }
        else {
            var index = $(this).index() + 1;
            if (index == 1)
                return;

            var table = $(this).closest('.main-grid');
            var row = $(this).closest('tr');

            var day = $(this).attr('data-day');
            if (day == null) {
                day = $('input:hidden[name=current_selected_day]').val();
            }

            if (table.hasClass('hours_on_top')) {
                var room = row.find('td:nth-child(1)').attr('data-room-id');
                var hour = $(this).attr('data-hour');
            }
            else {
                var hour = row.find('td:nth-child(1)').attr('data-hour');

                var room = $(this).attr('data-room-id');
                if (room == null) {
                    room = $('input:hidden[name=current_selected_room]').val();
                }
            }

            var data = {
                hour: hour,
                day: day,
                room: room
            };

            injectRemoteModal('/booking/create', data);
        }
    });

    $('body').on('focus', '.field.error', function() {
        $(this).removeClass('error');
    });

    $('body').on('click', '.dynamic-table .remove-row', function(e) {
        e.preventDefault();
        var table = $(this).closest('.dynamic-table');
        $(this).closest('tr').remove();

        var maxrows = parseInt(table.attr('data-max-rows'));
        if (maxrows != 0) {
            if (table.find('tbody tr:not(.reference-row)').length < maxrows) {
                table.find('tfoot').show();
            }
        }
    })
    .on('click', '.dynamic-table .add-row', function(e) {
        e.preventDefault();
        var table = $(this).closest('.dynamic-table');
        var row = table.find('.reference-row').clone();
        row.removeClass('reference-row').find('input:hidden[name*="id[]"]').val('new');
        table.find('> tbody').append(row);
        row.initWidgets();
    })
    .on('click', '.dynamic-table .add-remote-row', function(e) {
        e.preventDefault();
        var dtable = $(this).closest('.dynamic-table');
        var table = dtable.find('> tbody');
        var url = $(this).attr('data-remote-url');
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'HTML',
            success: function(data) {
                $(data).appendTo(table).initWidgets();
            }
        });
    });

    $('body').on('click', '.async-modal', function(e) {
        e.preventDefault();
        var url = $(this).attr('data-modal-url');
        injectRemoteModal(url, null);
    });

    $('body').on('click', '.async-portion', function(e) {
        e.preventDefault();
        var url = $(this).attr('data-portion-url');
        var container = $(this).attr('data-container-id');
        container = $('#' + container);

        $.ajax({
            url: url,
            method: 'GET',
            data: $(this).closest('form').serialize(),
            dataType: 'HTML',
            success: function(data) {
                container.empty().append(data).initWidgets();
            }
        });
    });

    $('body').on('submit', '.async-form', function(e) {
        e.preventDefault();
        var form = $(this);
        var button = $(this).find('button[type=submit]');

        button.prop('disabled', true);

        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: form.serialize(),
            success: function(data) {
                var dynamic_table = form.attr('data-fill-dynamic-table');
                if (dynamic_table) {
                    var table = $(dynamic_table);
                    adjustDynamicTable(table, data);
                }

                if (form.find('input:hidden[name=after_success]').length != 0) {
                    var func = form.find('input:hidden[name=after_success]').val();
                    window[func](data);
                }

                button.prop('disabled', false);
                form.closest('.modal').modal('hide');
            },
            error: function() {
                button.prop('disabled', false);
            }
        });
    });

    $('body').on('submit', '.reload-form', function(e) {
        e.preventDefault();
        var form = $(this);

        $.ajax({
            url: form.attr('action'),
            method: form.attr('method'),
            data: form.serialize(),
            success: function(data) {
                location.reload();
            }
        });
    });

    $('body').on('change', '.radio-loader input:radio', function() {
        if ($(this).prop('checked')) {
            var widget = $(this).closest('.radio-loader');
            var url = widget.attr('data-load-url');
            var area = widget.attr('data-load-area');
            $.ajax({
                method: 'GET',
                url: url,
                data: $(this).closest('form').serialize(),
                dataType: 'HTML',
                success: function(data) {
                    var d = $(data);
                    $(area).empty().append(d);
                    d.initWidgets();
                }
            });
        }
    });

    $('body').on('change', '.select-loader', function() {
        var url = $(this).find('option:selected').attr('data-load-url');
        var area = $(this).attr('data-load-area');
        $.ajax({
            method: 'GET',
            url: url,
            data: $(this).closest('form').serialize(),
            dataType: 'HTML',
            success: function(data) {
                var d = $(data);
                $(area).empty().append(d);
                d.initWidgets();
            }
        });
    });

    $('body').on('keyup', '.table_filter', function() {
        var table = $($(this).attr('data-filter-target'));
        var search = $(this).val();

        if (search == '') {
            table.find('tbody tr').show();
        }
        else {
            table.find('tbody tr').hide().filter(':contains_ci(' + search + ')').show();
        }
    });

    $('body').on('click', '.table_filter_button', function() {
        var table = $($(this).attr('data-filter-target'));
        var search = $(this).val();

        if (search == '') {
            table.find('tbody tr').show();
        }
        else {
            table.find('tbody tr').hide().filter(':contains_ci(' + search + ')').show();
        }
    });

    $('body').on('blur', '.remote-unique', function() {
        var input = $(this);
        var url = input.attr('data-unique-test');
        $.ajax({
            url: url,
            method: 'GET',
            dataType: 'JSON',
            data: {
                search: input.val(),
                exclude: input.attr('data-unique-test-exclude')
            },
            success: function(results) {
                if (results.results.length != 0) {
                    input.val('').closest('.field').addClass('error');
                }
            }
        });
    });

    $('body').on('change', '.ui.checkbox.explicit input:checkbox', function() {
        $(this).closest('.ui.checkbox.explicit').find('input:hidden').val($(this).prop('checked') ? '1' : '0');
    });

    var propagating_radios = false;
    $('body').on('change', 'input:radio.propagate_change', function() {
        if (propagating_radios)
            return;

        propagating_radios = true;
        var name = $(this).attr('name');
        $('body').find('input:radio[name=' + name + ']').not(this).change();
        propagating_radios = false;
    });

    $('body').on('change', '.ui.checkbox.displaying input:checkbox, .ui.checkbox.displaying input:radio', function() {
        var container = $(this).closest('.ui.checkbox.displaying');
        var target = container.attr('data-displaying-target');
        if (target)
            $(target).toggleClass('hidden', !$(this).prop('checked'));
        else
            $(this).closest('.field').find('.displayable').toggleClass('hidden', !$(this).prop('checked'));
    });

    $('body').initWidgets();

    /*
        Callbacks specifiche per contesto
    */

    $('body').on('change', '.customer-type-select input[type=radio]', function() {
        if ($(this).prop('checked')) {
            var is_person = $(this).val() == 'person';
            $(this).closest('.customer-base-registry-form').find('input:not([required])').prop('disabled', is_person);
            $('#contacts').attr('data-max-rows', is_person ? 1 : 99);
        }
    });

    $('body').on('added', '#contacts', function(e, data) {
        if ($(this).attr('data-max-rows') == '1') {
            var fullname = data.name + ' ' + data.surname;
            $(this).closest('form').find('.customer-base-registry-form input[name=name]').val(fullname);
        }
    });

    $('body').on('keyup', '.customer-filter', function() {
        var val = $(this).val().toLowerCase();
        var list = $(this).closest('.container').find('.customer-list');
        if (val == '') {
            list.find('.item').show();
        }
        else {
            list.find('.item').each(function() {
                if ($(this).find('a').text().toLowerCase().indexOf(val) == -1)
                    $(this).hide();
                else
                    $(this).show();
            });
        }
    });

    $('body').on('change', '.booking_cycle_select input:checkbox, .booking_cycle_select input:radio', function() {
        if ($(this).prop('checked') == false)
            return;

        var container = $(this).closest('.booking_cycle_select');
        var name = $(this).attr('name');
        var value = $(this).val();

        if (name == 'monthly_cycle[]') {
            if (value == 'all') {
                container.find('input[name="monthly_cycle[]"][value!="all"]').prop('checked', false);
            }
            else {
                container.find('input[name="monthly_cycle[]"][value="all"]').prop('checked', false);
                container.find('input[name="week_cycle"][value="none"]').prop('checked', true);
            }
        }
        else {
            container.find('input[name="monthly_cycle[]"]').prop('checked', false);
        }
    });

    $('body').on('change', '.booking_spaces input:not(.ignore-dates-check), .booking_spaces select', function(e) {
        $(this).closest('.booking_spaces').find('.test-booking-days').click();
    });

    $('body').on('keyup', '.booking_payments input[name=unit_price]', function() {
        var container = $(this).closest('.payments-wrapper');
        var price = $(this).val();
        var total = 0;

        var type = container.find('input:radio[name=payment]:checked').val();
        if (type == 'contribute') {
            total = price * container.find('input[name=partecipants]').val();
        }
        else if (type == 'pay') {
            $('#payments tr[data-involved_days]').each(function() {
                var days = $(this).attr('data-involved_days');
                $(this).find('input[name*=amount]').val(days * price);
            });

            $('#payments tr input[name*=amount]').each(function() {
                total += parseFloat($(this).val());
            });
        }

        $('.booking_payments input[name=total_price]').val(total);
    });
});
