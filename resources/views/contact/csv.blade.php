<div class="ui modal">
    <i class="close icon"></i>
    <div class="header">
        Importa CSV
    </div>
    <div class="content">
        <form method="POST" action="{{ route('contact.masscreate') }}" class="ui form" enctype="multipart/form-data">
            @csrf

            <p>
                Da qui è possibile importare un file CSV (<i>Comma Separated Values</i>) contenente i contatti desiderati.
            </p>
            <p>
                Il file deve avere, in quest'ordine, le colonne:
            </p>
            <ul>
                <li>Nome</li>
                <li>Cognome</li>
                <li>E-Mail</li>
                <li>E-Mail Secondaria</li>
                <li>Telefono</li>
                <li>Cellulare</li>
                <li>Codice Fiscale</li>
                <li>Indirizzo</li>
                <li>Città</li>
                <li>CAP</li>
                <li>Note</li>
            </ul>
            <p>
                Se viene trovato un contatto esistente con Nome e Cognome definiti nel CSV, i suoi attributi vengono aggiornati.
            </p>

            <div class="field">
                <label>File</label>
                <input type="file" name="file">
            </div>

            <button class="ui positive button" type="submit">Salva</button>
        </form>
    </div>
</div>
