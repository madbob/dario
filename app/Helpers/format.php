<?php

function decodeDate($date)
{
    list($day, $month, $year) = explode('/', $date);
    return sprintf('%s-%s-%s', $year, $month, $day);
}

function printablePrice($price)
{
    return sprintf('%.02f', round($price, 2));
}
