@if($grant)
    <input type="hidden" name="grant_id" value="{{ $grant->id }}">
@endif

<div class="two fields">
    <div class="required field">
        <label>Descrizione Assegnazione</label>
        <input type="text" name="grant_description" value="{{ $grant ? $grant->description : '' }}" data-required="true">
    </div>

    <div class="required field">
        <label>Tipo</label>
        <div class="inline fields">
            @foreach(App\Grant::types() as $identifier => $info)
                <div class="field">
                    <div class="ui radio checkbox">
                        <input type="radio" name="grant_type" value="{{ $identifier }}" class="hidden" data-required="true" {{ $grant && $grant->type == $identifier ? 'checked' : '' }}>
                        <label>{{ $info->label }}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

<div class="four fields">
    <div class="required field">
        <label>Anno Sociale</label>
        @if($grant)
            <p>
                <input type="hidden" name="year" value="{{ $grant->year }}">
                {{ $grant->yearobj->name }}
            </p>
        @else
            <select name="grant_year" data-required="true">
                <?php $current_social_year = currentSocialYear() ?>
                @foreach(App\Year::orderBy('name', 'desc')->get() as $iter_year)
                    <option value="{{ $iter_year->id }}" {{ $current_social_year->id == $iter_year->id ? 'selected' : '' }}>{{ $iter_year->name }}</option>
                @endforeach
            </select>
        @endif
    </div>

    <div class="field">
        <label>Numero Protocollo</label>
        <input type="number" name="grant_number" value="{{ $grant ? $grant->number : App\Grant::max('number') + 1 }}">
    </div>

    <div class="field ui calendar">
        <label>Data Protocollo</label>
        <input type="text" name="grant_protocol" value="{{ $grant ? $grant->protocol : date('Y-m-d') }}">
    </div>

    <div class="field ui calendar">
        <label>Data Firma</label>
        <input type="text" name="grant_signature" value="{{ $grant ? $grant->signature : '' }}">
    </div>
</div>

@if($customer)
    @include('customer.contactselect', ['customer' => $customer, 'selected_id' => $grant ? $grant->contact_id : 0])
@endif

<div class="field">
    <label>Note Assegnazione</label>
    <textarea name="grant_notes" rows="3">{{ $grant ? $grant->notes : '' }}</textarea>
</div>
