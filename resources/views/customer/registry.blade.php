<?php

if(!isset($prefix))
    $prefix = '';

?>

<div class="customer-base-registry-form">
    <div class="two fields">
        <div class="required six wide field">
            <label>Nome</label>
            <input type="text" name="{{ $prefix }}name" value="{{ $customer ? $customer->name : '' }}" class="remote-unique" data-unique-test="{{ route('customer.search') }}" data-unique-test-exclude="{{ $customer ? $customer->id : '' }}" required>
        </div>

        <div class="required ten wide field customer-type-select">
            <label>Tipo</label>
            <div class="inline fields">
                <div class="field">
                    <div class="ui radio checkbox {{ $customer && $customer->type == 'private' ? 'active' : '' }}">
                        <input type="radio" name="{{ $prefix }}type" value="private" class="hidden" {{ $customer && $customer->type == 'private' ? 'checked' : '' }} required>
                        <label>Privato</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox {{ $customer && $customer->type == 'association' ? 'active' : '' }}">
                        <input type="radio" name="{{ $prefix }}type" value="association" class="hidden" {{ $customer && $customer->type == 'association' ? 'checked' : '' }} required>
                        <label>Associazione</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox {{ $customer && $customer->type == 'cooperative' ? 'active' : '' }}">
                        <input type="radio" name="{{ $prefix }}type" value="cooperative" class="hidden" {{ $customer && $customer->type == 'cooperative' ? 'checked' : '' }} required>
                        <label>Cooperativa</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox {{ $customer && $customer->type == 'group' ? 'active' : '' }}">
                        <input type="radio" name="{{ $prefix }}type" value="group" class="hidden" {{ $customer && $customer->type == 'group' ? 'checked' : '' }} required>
                        <label>Gruppo</label>
                    </div>
                </div>
                <div class="field">
                    <div class="ui radio checkbox {{ $customer && $customer->type == 'person' ? 'active' : '' }}">
                        <input type="radio" name="{{ $prefix }}type" value="person" class="hidden" {{ $customer && $customer->type == 'person' ? 'checked' : '' }} required>
                        <label>Persona Fisica</label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($customer == null || $customer->type != 'person')
        <div class="three fields">
            <div class="field">
                <label>Indirizzo</label>
                <input type="text" name="{{ $prefix }}address" value="{{ $customer ? $customer->address : '' }}">
            </div>
            <div class="field">
                <label>Città</label>
                <input type="text" name="{{ $prefix }}city" value="{{ $customer ? $customer->city : '' }}">
            </div>
            <div class="field">
                <label>CAP</label>
                <input type="text" name="{{ $prefix }}zip" value="{{ $customer ? $customer->zip : '' }}">
            </div>
        </div>

        <div class="two fields">
            <div class="field">
                <label>Codice Fiscale</label>
                <input type="text" name="{{ $prefix }}taxcode" value="{{ $customer ? $customer->taxcode : '' }}">
            </div>
            <div class="field">
                <label>Partita IVA</label>
                <input type="text" name="{{ $prefix }}vat" value="{{ $customer ? $customer->vat : '' }}">
            </div>
        </div>

        <div class="three fields">
            <div class="field">
                <label>Telefono</label>
                <input type="text" name="{{ $prefix }}phone" value="{{ $customer ? $customer->phone : '' }}">
            </div>
            <div class="field">
                <label>E-Mail</label>
                <input type="email" name="{{ $prefix }}email" value="{{ $customer ? $customer->email : '' }}">
            </div>
            <div class="field">
                <label>Website</label>
                <input type="text" name="{{ $prefix }}website" value="{{ $customer ? $customer->website : '' }}">
            </div>
        </div>
    @endif

    <div class="field">
        <label>Note Cliente</label>
        <textarea name="{{ $prefix }}note" rows="3">{{ $customer ? $customer->notes : '' }}</textarea>
    </div>
</div>
