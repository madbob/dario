<?php

$column_num = $rooms->count() + 1;

$filled = [];
foreach($rooms as $room) {
    $filled[$room->id] = [];
    for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
        $filled[$room->id][(string) $i] = false;
}

?>

@if($show_day)
    <tr>
        <td class="day-separator" colspan="{{ $column_num }}" data-day="{{ $day->format('Y-m-d') }}">{{ $day->format('l d/m') }}</td>
    </tr>
@endif

@for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
    <tr>
        <td data-hour="{{ $i }}">{{ $t = floatToTime($i) }}</td>

        @foreach($rooms as $room)
            @if($filled[$room->id][(string) $i])
                @continue
            @endif

            @if($slot = App\Slot::where('room_id', $room->id)->where('date', $day)->where('from', $t)->where('abolished', false)->first())
                @php
                    $duration = $slot->duration_cells;
                    for($void = $i; $void < ($i + ($duration / 2)); $void += 0.5)
                        $filled[$room->id][(string) $void] = true;
                @endphp

                @include('slot.cell', ['slot' => $slot])
            @else
                <td data-room-id="{{ $room->id }}">&nbsp;</td>
            @endif
        @endforeach
    </tr>
@endfor
