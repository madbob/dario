@extends('layouts.app')

@section('content')
<style type="text/css">
    .column {
        max-width: 450px;
    }
</style>

<script>

$(document).ready(function() {
    $('.ui.form').form({
        fields: {
            email: {
                identifier  : 'email',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Inserisci il tuo indirizzo e-mail'
                    },
                    {
                        type   : 'email',
                        prompt : 'Inserisci un indirizzo e-mail valido'
                    }
                ]
            },
            password: {
                identifier  : 'password',
                rules: [
                    {
                        type   : 'empty',
                        prompt : 'Inserisci la tua password'
                    }
                ]
            }
        }
    });
});
</script>

<div class="ui middle aligned center aligned grid">
    <div class="column">
        @if(file_exists(public_path('images/logo.png')))
            <p>
                <img src="{{ asset('images/logo.png') }}">
            </p>
        @endif

        <h2 class="ui teal image header">
            <div class="content">Login</div>
        </h2>

        <form class="ui large form" method="POST" action="{{ route('login') }}">
            @csrf

            <input type="hidden" name="remember" value="1">

            <div class="ui stacked segment">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="user icon"></i>
                        <input type="text" name="email" placeholder="Indirizzo E-Mail" required autofocus>
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="lock icon"></i>
                        <input type="password" name="password" placeholder="Password" required>
                    </div>
                </div>
                <div class="ui fluid large teal submit button">Login</div>
            </div>

            <div class="ui error message"></div>
        </form>
    </div>
</div>
@endsection
