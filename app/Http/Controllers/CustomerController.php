<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Customer;
use App\Contact;
use App\Grant;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $display = $request->input('display', 'recent');

        if ($display == 'all') {
            $customers = Customer::orderBy('name', 'asc')->get();
        }
        else {
            $customers = Customer::orderBy('name', 'asc')->recent()->get();
        }

        return view('customer.index', compact('customers', 'display'));
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        $ret = (object) [
            'results' => []
        ];

        $query = Customer::where('name', 'like', "%$search%");

        if ($request->has('exclude') && !empty($request->input('exclude')))
            $query->where('id', '!=', $request->input('exclude'));

        $customers = $query->get();

        foreach($customers as $customer) {
            $ret->results[] = (object) [
                'title' => $customer->name,
                'id' => $customer->id,
            ];
        }

        return response()->json($ret);
    }

    private function readGrants($customer, $request)
    {
        $ids = $request->input('grant_id');
        $saved = [];

        if (is_array($ids)) {
            foreach($ids as $index => $id) {
                if (empty($id)) {
                    continue;
                }

                $saved[] = $id;
            }
        }

        Grant::where('customer_id', $customer->id)->whereNotIn('id', $saved)->delete();
    }

    public function create(Request $request)
    {
        $customer = null;
        return view('customer.edit', compact('customer'));
    }

    public function store(Request $request)
    {
        $customer = new Customer();
        $customer->readRequest($request);
        $customer->save();
        $customer->readContacts($request, 'contact_id');
        return redirect()->route('customer.index');
    }

    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('customer.edit', compact('customer'));
    }

    public function update(Request $request, $id)
    {
        $customer = Customer::find($id);
        $customer->readRequest($request);
        $customer->save();
        $customer->readContacts($request, 'contact_id');
        self::readGrants($customer, $request);
        return redirect()->route('customer.index');
    }

    public function contacts(Request $request)
    {
        $id = $request->input('customer_id');
        $customer = Customer::find($id);
        return view('customer.contactselect', compact('customer'));
    }

    public function askdestroy($id)
    {
        $customer = Customer::find($id);
        return view('customer.askdestroy', compact('customer'));
    }

    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();
        Contact::whereDoesntHave('customers')->delete();
        return redirect()->route('customer.index');
    }
}
