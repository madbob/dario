<?php

$r = App\Room::find($room);
if ($r)
    $equipments = $r->equipments;
else
    $equipments = [];

if (!isset($selected) || is_null($selected))
    $selected = [];

?>

<div class="field equipment-select">
    @foreach($equipments as $equipment)
        <div class="ui checkbox">
            <input type="checkbox" name="equipment_id[]" value="{{ $equipment->id }}" class="hidden" {{ in_array($equipment->id, $selected) ? 'checked' : '' }}>
            <label>{{ $equipment->name }}</label>
        </div>
    @endforeach
</div>
