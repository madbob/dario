<div class="ui modal">
    <i class="close icon"></i>
    <div class="header">
        @if($contact)
            Modifica Contatto
        @else
            Crea Contatto
        @endif
    </div>
    <div class="content">
        @if($contact)
            <div class="ui message">
                <p>
                    Contatto coinvolto in: {{ join(', ', $contact->customers()->pluck('name')->toArray()) }}
                </p>
            </div>

            <div class="ui divider"></div>
        @endif

        @include('contact.edit', ['contact' => $contact, 'embedded' => true])
    </div>
</div>
