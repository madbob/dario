<div class="required field">
    <label>Sala</label>
    <select class="select-loader" data-load-area=".equipment-select" name="room_id" required>
        @foreach($currentuser->rooms()->orderBy('name', 'asc')->get() as $room)
            <option value="{{ $room->id }}" {{ $selected_room == $room->id ? 'selected' : '' }} data-load-url="{{ route('room.equipment', $room->id) }}">{{ $room->name_with_types }}</option>
        @endforeach
    </select>
</div>

@include('room.equipmentselect', ['room' => $selected_room, 'selected' => $selected_equipment])

<div class="four fields">
    <div class="required field ui calendar" data-change-function="verifyEndDate" data-end-widget="#endDate">
        <label>Dal Giorno</label>
        <input type="text" name="start_day" value="{{ $selected_day ?: date('Y-m-d') }}">
    </div>
    <div class="field ui calendar" id="endDate">
        <label><input type="radio" name="end_criteria" value="given_day" {{ $details->end->criteria == 'given_day' ? 'checked' : '' }}> Al Giorno</label>
        <input type="text" name="end_day" value="{{ $details->end->criteria == 'given_day' ? $details->end->date : '' }}" autocomplete="off">
    </div>
    <div class="field">
        <label><input type="radio" name="end_criteria" value="given_number" {{ $details->end->criteria == 'given_number' ? 'checked' : '' }}> Numero di Occorrenze</label>
        <input type="number" name="given_number_value" value="{{ $details->end->criteria == 'given_number' ? $details->end->number : '' }}" autocomplete="off">
    </div>
    <div class="field">
        <label><input type="radio" name="end_criteria" value="all_year" {{ $details->end->criteria == 'all_year' ? 'checked' : '' }}> Tutto l'Anno</label>
        <input type="text" value="Fino al {{ writeDate(currentSocialYear()->end) }}" disabled>
    </div>
</div>

<div class="field booking_cycle_select">
    <label>Ciclicità Settimanale</label>
    <div class="inline fields">
        <div class="field">
            <div class="ui checkbox active">
                <input type="checkbox" name="monthly_cycle[]" value="all" class="hidden" {{ in_array('all', $details->cycle) ? 'checked' : '' }}>
                <label>Tutte</label>
            </div>
        </div>
        <div class="field">
            <div class="ui checkbox">
                <input type="checkbox" name="monthly_cycle[]" value="first" class="hidden" {{ in_array('first', $details->cycle) ? 'checked' : '' }}>
                <label>Prima del Mese</label>
            </div>
        </div>
        <div class="field">
            <div class="ui checkbox">
                <input type="checkbox" name="monthly_cycle[]" value="second" class="hidden" {{ in_array('second', $details->cycle) ? 'checked' : '' }}>
                <label>Seconda del Mese</label>
            </div>
        </div>
        <div class="field">
            <div class="ui checkbox">
                <input type="checkbox" name="monthly_cycle[]" value="third" class="hidden" {{ in_array('third', $details->cycle) ? 'checked' : '' }}>
                <label>Terza del Mese</label>
            </div>
        </div>
        <div class="field">
            <div class="ui checkbox">
                <input type="checkbox" name="monthly_cycle[]" value="fourth" class="hidden" {{ in_array('fourth', $details->cycle) ? 'checked' : '' }}>
                <label>Quarta del Mese</label>
            </div>
        </div>
        <div class="field">
            <div class="ui checkbox">
                <input type="checkbox" name="monthly_cycle[]" value="last" class="hidden" {{ in_array('last', $details->cycle) ? 'checked' : '' }}>
                <label>Ultima del Mese</label>
            </div>
        </div>
    </div>
    <div class="inline fields">
        <div class="field">
            <div class="ui radio checkbox active">
                <input type="radio" name="week_cycle" value="none" class="hidden" {{ in_array('all', $details->cycle) ? 'checked' : '' }}>
                <label>Ignora</label>
            </div>
        </div>
        <div class="field">
            <div class="ui radio checkbox">
                <input type="radio" name="week_cycle" value="2_weeks" class="hidden" {{ in_array('2_weeks', $details->cycle) ? 'checked' : '' }}>
                <label>Ogni 2 Settimane</label>
            </div>
        </div>
        <div class="field">
            <div class="ui radio checkbox">
                <input type="radio" name="week_cycle" value="3_weeks" class="hidden" {{ in_array('3_weeks', $details->cycle) ? 'checked' : '' }}>
                <label>Ogni 3 Settimane</label>
            </div>
        </div>
        <div class="field">
            <div class="ui radio checkbox">
                <input type="radio" name="week_cycle" value="4_weeks" class="hidden" {{ in_array('4_weeks', $details->cycle) ? 'checked' : '' }}>
                <label>Ogni 4 Settimane</label>
            </div>
        </div>
    </div>
</div>

<table id="days_selector" class="ui very basic table dynamic-table">
    <tbody>
        @foreach($details->days as $d)
            @include('booking.spaces.recurring_weekly_day', ['visible' => true, 'day' => $d->offset, 'from' => $d->from, 'to' => $d->to])
        @endforeach

        @include('booking.spaces.recurring_weekly_day', ['visible' => false, 'day' => -1, 'from' => $details->days[0]->from, 'to' => $details->days[0]->to])
    </tbody>
    <tfoot>
        <tr>
            <td>
                <button class="ui orange basic button add-row" data-table-target="#rooms">Aggiungi Giorno della Settimana</button>
                <a class="ui basic button test-booking-days async-portion" href="#" data-portion-url="{{ route('booking.wizard', ['page' => 'test']) }}" data-container-id="days_tester">Verifica Giorni</a>
            </td>
        </tr>
    </tfoot>
</table>

@if(isset($booking))
    <?php $abolished = $booking->slots()->where('abolished', true)->get() ?>

    @if(!empty($booking->excluded_days) || $abolished->count() != 0)
        @if(!empty($booking->excluded_days))
            <p>
                Giorni esclusi: {{ join(', ', array_map(function($d) {
                    return fullDate($d);
                }, $booking->excluded_days)) }}.
            </p>
        @endif

        @if($abolished->count() != 0)
            <p>
                Giorni rimossi manualmente: {{ join(', ', $abolished->reduce(function($c, $s) {
                    $c[] = fullDate($s->date);
                    return $c;
                }, [])) }}.
            </p>
        @endif

        <p>
            Clicca "Verifica Giorni" per modificare.
        </p>
    @endif
@endif

<div id="days_tester"></div>
