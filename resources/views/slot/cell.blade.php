<td class="occupied" data-slot-url="{{ route('slot.edit', $slot->id) }}" rowspan="{{ $slot->duration_cells }}">
    <div class="{{ $slot->grid_class }}">
        <span class="grant-name">{{ $slot->booking->printable_description }}</span>
        <span class="customer-name">{{ $slot->booking->grant->customer->name }}</span>
    </div>
</td>
