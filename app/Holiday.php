<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    public function nextDate()
    {
        if ($this->name == 'Pasqua') {
            $year = (new \DateTime($this->date))->modify('+1 years')->format('Y');
            return (new \DateTime())->setTimestamp(easter_date($year));
        }
        else if ($this->name == 'Pasquetta') {
            $year = (new \DateTime($this->date))->modify('+1 years')->format('Y');
            return (new \DateTime())->setTimestamp(easter_date($year) + (60 * 60 * 24));
        }
        else {
            return (new \DateTime($this->date))->modify('+1 years');
        }
    }
}
