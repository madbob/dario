<tr class="{{ $user ? '' : 'reference-row' }}">
    <td>
        <input type="hidden" name="id[]" value="{{ $user ? $user->id : 'empty' }}">
        <div class="required field">
            <input type="text" name="name[]" value="{{ $user ? $user->name : '' }}">
        </div>
    </td>
    <td>
        <div class="field">
            <input type="email" name="email[]" value="{{ $user ? $user->email : '' }}">
        </div>
    </td>
    <td>
        <div class="field">
            <input type="password" name="password[]">
        </div>
    </td>
    <td>
        <div class="grouped fields">
            @foreach(App\Room::orderBy('name', 'asc')->get() as $room)
                <?php $checked = ($user && $user->rooms()->where('rooms.id', $room->id)->count() != 0) ?>
                <div class="field">
                    <div class="ui {{ $checked ? 'checked' : '' }} checkbox explicit">
                        <input type="checkbox" name="{{ \Illuminate\Support\Str::random(10) }}" tabindex="0" class="hidden" {{ $checked ? 'checked' : '' }}>
                        <input type="hidden" name="room_{{ $room->id }}[]" value="{{ $checked ? '1' : '0' }}">
                        <label>{{ $room->name }}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </td>
    <td>
        <div class="ui toggle checkbox explicit {{ $user && $user->role == 'admin' ? 'checked' : '' }}">
            <input type="checkbox" name="{{ \Illuminate\Support\Str::random(10) }}" tabindex="0" class="hidden" {{ $user && $user->role == 'admin' ? 'checked' : '' }}>
            <input type="hidden" name="admin[]" value="{{ $user && $user->role == 'admin' ? '1' : '0' }}">
        </div>
    </td>
    <td>
        <button class="ui red icon button pull-right remove-row"><i class="trash icon"></i></button>
    </td>
</tr>
