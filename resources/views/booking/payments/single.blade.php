<div class="field">
    <label>Totale Giorni</label>
    <div class="ui right input">
        {{ count($days) }}
    </div>
</div>

<div class="two fields">
    <div class="required field">
        <label>Prezzo Unitario</label>
        <div class="ui right labeled input">
            <input type="number" step="0.01" name="unit_price" value="{{ $unit_price }}" required>
            <div class="ui basic label">€</div>
        </div>
    </div>

    <div class="required field">
        <label>Prezzo Totale</label>
        <div class="ui right labeled input">
            <input type="number" step="0.01" name="total_price" value="{{ $total_price }}" required>
            <div class="ui basic label">€</div>
        </div>
    </div>
</div>

<div class="two fields">
    <div class="required field">
        <label>Scadenza</label>
        <div class="ui calendar">
            <input type="text" name="payment_date" value="{{ $payment ? $payment->date : '' }}" required>
        </div>
    </div>

    <div class="field">
        <label>Pagato</label>
        <div class="ui toggle checkbox">
            <input type="checkbox" name="payment_payed" {{ $payment && $payment->payed ? 'checked' : '' }}>
        </div>
    </div>
</div>
