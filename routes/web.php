<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('booking/wizard/{page}', 'BookingController@wizard')->name('booking.wizard');
    Route::get('booking/askdestroy/{id}', 'BookingController@askdestroy')->name('booking.askdestroy');

    Route::get('room/equipment/{id}', 'RoomController@equipment')->name('room.equipment');

    Route::get('slot/askdestroy/{id}', 'SlotController@askdestroy')->name('slot.askdestroy');

    Route::get('grant/create/search_customer', 'GrantController@creatingByExistingCustomer')->name('grant.search_customer');
    Route::get('grant/create/new_customer', 'GrantController@creatingByNewCustomer')->name('grant.new_customer');
    Route::get('grant/document/{id}', 'GrantController@document')->name('grant.document');
    Route::get('grant/askdestroy/{id}', 'GrantController@askdestroy')->name('grant.askdestroy');

    Route::get('contact/askmasscreate', 'ContactController@askCSV')->name('contact.askmasscreate');
    Route::post('contact/masscreate', 'ContactController@importCSV')->name('contact.masscreate');
    Route::get('contact/search', 'ContactController@search')->name('contact.search');
    Route::get('contact/askdestroy/{id}', 'ContactController@askdestroy')->name('contact.askdestroy');

    Route::get('customer/search', 'CustomerController@search')->name('customer.search');
    Route::get('customer/contacts', 'CustomerController@contacts')->name('customer.contacts');
    Route::get('customer/askdestroy/{id}', 'CustomerController@askdestroy')->name('customer.askdestroy');

    Route::post('config/generic', 'ConfigController@generic')->name('config.generic');
    Route::post('config/users', 'ConfigController@users')->name('config.users');
    Route::post('config/equipments', 'ConfigController@equipments')->name('config.equipments');
    Route::post('config/review', 'ConfigController@review')->name('config.review');

    Route::resource('config', 'ConfigController');
    Route::resource('year', 'YearController');
    Route::resource('holiday', 'HolidayController');
    Route::resource('room', 'RoomController');
    Route::resource('customer', 'CustomerController');
    Route::resource('contact', 'ContactController');
    Route::resource('grant', 'GrantController');
    Route::resource('slot', 'SlotController');
    Route::resource('booking', 'BookingController');
});
