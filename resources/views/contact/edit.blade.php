<form id="contact-form" method="POST" action="{{ $contact ? route('contact.update', $contact->id) : route('contact.store') }}" class="ui form {{ $embedded ? 'async-form' : '' }}" data-fill-dynamic-table="#contacts">
    @csrf

    @if($contact)
        @method('PUT')
    @else
        @if($embedded)
            <div class="required field ui search autocompletable" data-autocomplete-url="/contact/search/?search={query}" data-autocomplete-target="#contact-form">
                <input type="hidden" name="id" value="new" data-autocompletable-map="id">
                <label>Ricerca</label>
                <div class="ui icon input">
                    <input type="text" name="name" class="prompt">
                    <i class="inverted circular undo link icon"></i>
                </div>
                <div class="results"></div>
                <p>Scrivi ed autocompleta il nome di un contatto esistente per aggiungerlo anche a questo cliente.</p>
            </div>
        @endif
    @endif

    @if($embedded == false)
        <input type="hidden" name="full_update" value="1">
    @endif

    <div class="two fields">
        <div class="required field">
            <label>Nome</label>
            <input type="text" name="name" value="{{ $contact ? $contact->name : '' }}" required data-autocompletable-map="name">
        </div>
        <div class="required field">
            <label>Cognome</label>
            <input type="text" name="surname" value="{{ $contact ? $contact->surname : '' }}" required data-autocompletable-map="surname">
        </div>
    </div>

    <div class="field">
        <label>Indirizzo</label>
        <input type="text" name="address" value="{{ $contact ? $contact->address : '' }}" data-autocompletable-map="address">
    </div>

    <div class="two fields">
        <div class="field">
            <label>Città</label>
            <input type="text" name="city" value="{{ $contact ? $contact->city : '' }}" data-autocompletable-map="city">
        </div>
        <div class="field">
            <label>CAP</label>
            <input type="text" name="zip" value="{{ $contact ? $contact->zip : '' }}" data-autocompletable-map="zip">
        </div>
    </div>

    <div class="field">
        <label>Codice Fiscale</label>
        <input type="text" name="taxcode" value="{{ $contact ? $contact->taxcode : '' }}" data-autocompletable-map="taxcode">
    </div>

    <div class="two fields">
        <div class="field">
            <label>Telefono</label>
            <input type="text" name="phone" value="{{ $contact ? $contact->phone : '' }}" data-autocompletable-map="phone">
        </div>
        <div class="field">
            <label>Cellulare</label>
            <input type="text" name="mobile" value="{{ $contact ? $contact->mobile : '' }}" data-autocompletable-map="mobile">
        </div>
    </div>

    <div class="two fields">
        <div class="field">
            <label>E-Mail</label>
            <input type="email" name="email" value="{{ $contact ? $contact->email : '' }}" data-autocompletable-map="email">
        </div>
        <div class="field">
            <label>E-Mail Secondaria</label>
            <input type="email" name="email2" value="{{ $contact ? $contact->email2 : '' }}" data-autocompletable-map="email2">
        </div>
    </div>

    <div class="field">
        <label>Note</label>
        <textarea name="notes" rows="3" data-autocompletable-map="notes">{{ $contact ? $contact->notes : '' }}</textarea>
    </div>

    @if($contact && $embedded == false)
        <h4 class="ui horizontal divider header">
            <i class="phone icon"></i>
            Clienti
        </h4>

        @if($contact->customers->count() == 0)
            <div class="ui message">
                Non ci sono clienti assegnati a questo contatto.
            </div>
        @else
            <table id="customers" class="ui basic table dynamic-table">
                <thead>
                    <tr>
                        <th width="40%">Nome</th>
                        <th width="40%">Contatti Totali</th>
                        <th width="20%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($contact->customers as $customer)
                        <tr>
                            <td>
                                <input type="hidden" name="customer_id[]" value="{{ $customer->id }}">
                                {{ $customer->name }}
                            </td>
                            <td>{{ $customer->contacts()->count() }}</td>
                            <td>
                                <button class="ui red icon button pull-right remove-row"><i class="trash icon"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        @endif
    @endif

    <button class="ui positive button" type="submit">Salva</button>
    @if($contact && $embedded == false)
        <a class="ui negative button right floated async-modal" href="#" data-modal-url="{{ route('contact.askdestroy', $contact->id) }}">Elimina</a>
    @endif
</form>
