<div class="ui mini modal">
    <i class="close icon"></i>
    <div class="header">
        Elimina Prenotazione
    </div>
    <div class="content">
        <p>
            Sei sicuro di voler eliminare la prenotazione di {{ $booking->grant->customer->name }}?
        </p>

        <form method="POST" action="{{ route('booking.destroy', $booking->id) }}" class="ui form">
            @csrf
            @method('DELETE')
            <input type="hidden" name="mode" value="all">
            <button class="ui negative button" type="submit">Elimina Tutto</button>
            <p>
                Elimina la prenotazione e tutte le relative occupazioni della sala.
            </p>
        </form>

        @if($booking->slots()->where('date', '>', date('Y-m-d'))->count() > 0)
            <div class="ui divider"></div>

            <form method="POST" action="{{ route('booking.destroy', $booking->id) }}" class="ui form">
                @csrf
                @method('DELETE')
                <input type="hidden" name="mode" value="partial">
                <button class="ui negative button" type="submit">Annulla Prenotazione</button>
                <p>
                    Annulla l'occupazione della sala a partire da domani, la prenotazione si conclude oggi.
                </p>
            </form>
        @endif
    </div>
</div>
