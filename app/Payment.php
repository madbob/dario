<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Events\SluggableCreating;

use App\Booking;

class Payment extends Model
{
    use SluggableID;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $dispatchesEvents = [
        'creating' => SluggableCreating::class,
    ];

    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }

    public function getQuarterDaysAttribute()
    {
        if ($this->quarter == 'none')
            return 0;

        $q = $this->quarter;
        $quarters = Booking::sortDaysByQuarter($this->booking->getDays(false));
        return $quarters->$q->days;
    }

    public function getPrintableFullDescriptionAttribute()
    {
        if ($this->payed)
            $status = 'green';
        else if ($this->date < date('Y-m-d'))
            $status = 'red';
        else
            $status = 'orange';

        return sprintf('<div class="ui %s horizontal label">%s - %s - %s€</div>', $status, $this->description, writeDate($this->date), $this->amount);
    }

    public static function saveBatch($request, $booking = null)
    {
        $payments = $request->input('payment_id');
        $payments_description = $request->input('payment_description');
        $payments_quarter = $request->input('payment_quarter');
        $payments_amount = $request->input('payment_amount');
        $payments_date = $request->input('payment_date');
        $payments_payed = $request->input('payment_payed');

        if ($payments) {
            foreach($payments as $index => $payment) {
                if (empty($payment) || $payment == 'empty') {
                    $pay = new Payment();
                    $pay->booking_id = $booking->id;
                }
                else {
                    $pay = Payment::find($payment);
                }

                if ($pay == null) {
                    $pay = new Payment();
                    $pay->booking_id = $booking->id;
                    if (!empty($payment) && $payment != 'empty')
                        $pay->id = $payment;
                }

                $amount = $payments_amount[$index];

                if ($amount == 0) {
                    if ($pay->exists)
                        $pay->delete();
                    continue;
                }

                $pay->description = $payments_description[$index];
                $pay->quarter = $payments_quarter[$index];
                $pay->amount = $amount;
                $pay->date = readDate($payments_date[$index]);
                $pay->payed = $payments_payed[$index];
                $pay->save();
            }
        }
    }

    /************************************************************ SluggableID */

    public function getSluggableID()
    {
        return sprintf('%s %s', $this->booking->grant->description, $this->date);
    }
}
