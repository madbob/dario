<div class="field">
    <label>Totale Giorni</label>
    <div class="ui right input">
        {{ count($days) }}
    </div>
</div>

<div class="two fields">
    <div class="field">
        <label>Prezzo Giornaliero</label>
        <div class="ui right labeled input">
            <input type="number" step="0.01" name="unit_price" value="{{ $unit_price }}">
            <div class="ui basic label">€</div>
        </div>
    </div>

    <div class="field">
        <label>Prezzo Totale</label>
        <div class="ui right labeled input">
            <input type="number" step="0.01" name="total_price" value="{{ $total_price }}">
            <div class="ui basic label">€</div>
        </div>
    </div>
</div>

<table id="payments" class="ui basic table dynamic-table">
    <thead>
        <tr>
            <th width="30%">Descrizione</th>
            <th width="10%">Giorni</th>
            <th width="20%">Prezzo</th>
            <th width="20%">Scadenza</th>
            <th width="10%">Pagato</th>
            <th width="10%">&nbsp;</th>
        </tr>
    </thead>
    <tbody>
        @foreach($payments as $payment)
            @include('booking.payments.payment', ['payment' => $payment, 'has_days' => true])
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="2">
                <button class="ui orange basic button add-remote-row" data-remote-url="{{ route('booking.wizard', ['page' => 'single-payment']) }}">Aggiungi Pagamento</button>
            </td>
        </tr>
    </tfoot>
</table>
