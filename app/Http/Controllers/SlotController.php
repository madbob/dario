<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Slot;

class SlotController extends Controller
{
    public function edit($id)
    {
        $slot = Slot::find($id);
        return view('slot.edit', compact('slot'));
    }

    public function update(Request $request, $id)
    {
        $slot = Slot::find($id);
        $slot->room_id = $request->input('room_id');
        $slot->date = new \DateTime(readDate($request->input('date')));
        $slot->from = floatToTime($request->input('from'));
        $slot->to = floatToTime($request->input('to'));
        $slot->save();
    }

    public function askdestroy($id)
    {
        $slot = Slot::find($id);
        return view('slot.askdestroy', compact('slot'));
    }

    public function destroy($id)
    {
        $slot = Slot::find($id);
        $slot->delete();
        return redirect()->back();
    }
}
