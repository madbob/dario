<tr class="{{ $visible ? '' : 'reference-row' }}">
    <td>
        <select name="day[]">
            <option value="-1" {{ -1 == $day ? 'selected' : '' }}>Seleziona Giorno</option>

            @foreach(days() as $a => $dname)
                <option value="{{ $a }}" {{ $a == $day ? 'selected' : '' }}>{{ $dname }}</option>
            @endforeach
        </select>
    </td>
    <td>
        <select name="from[]">
            @for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
                <?php $t = floatToTime($i) ?>
                <option value="{{ $i }}" {{ floatToTime($from) == $t ? 'selected' : '' }}>{{ $t }}</option>
            @endfor
        </select>
    </td>
    <td>
        <select name="to[]">
            @for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
                <?php $t = floatToTime($i) ?>
                <option value="{{ $i }}" {{ floatToTime($to) == $t ? 'selected' : '' }}>{{ $t }}</option>
            @endfor
        </select>
    </td>
    <td>
        <button class="ui red icon button remove-row"><i class="trash icon"></i></button>
    </td>
</tr>
