<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

use Log;

use App\Events\SluggableCreating;

class Grant extends Model
{
    use SluggableID;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $dispatchesEvents = [
        'creating' => SluggableCreating::class,
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

    public function bookings()
    {
        return $this->hasMany('App\Booking')->orderBy('start', 'asc');
    }

    public function yearobj()
    {
        return $this->belongsTo('App\Year', 'year');
    }

    public function getPaymentsAttribute()
    {
        $payments = new Collection();

        foreach($this->bookings as $booking)
            $payments = $payments->merge($booking->payments);

        $payments->sortBy('date');
        return $payments;
    }

    public static function types()
    {
        return [
            'party' => (object) [
                'label' => 'Festa'
            ],
            'course' => (object) [
                'label' => 'Corso'
            ],
            'internal' => (object) [
                'label' => 'Interno'
            ],
            'other' => (object) [
                'label' => 'Altro'
            ],
        ];
    }

    public function getHumanTypeAttribute()
    {
        $types = self::types();

        if (isset($types[$this->type])) {
            return $types[$this->type]->label;
        }
        else {
            Log::error('Assegnazione di tipo non identificato: ' . $this->type);
            return '?';
        }
    }

    public function getHumanInvolvedTypesAttribute()
    {
        $types = [];

        foreach($this->bookings as $booking) {
            switch($booking->type) {
                case 'recurring':
                    $types[] = 'Ricorrente';
                    break;
                case 'occasional':
                    $types[] = 'Occasionale';
                    break;
                default:
                    Log::error('Prenotazione di tipo non identificato: ' . $this->type);
                    break;
            }
        }

        return join(', ', array_unique($types));
    }

    public function getYearHumanSummaryAttribute()
    {
        $tokens = [];

        foreach($this->bookings as $booking) {
            $recursion = $booking->recursion_obj;
            if (isset($recursion->end->yearly)) {
                $tokens[] = $booking->year_human_summary;
            }
        }

        return join("\n", $tokens);
    }

    public function getAddressAttribute()
    {
        $data = [];

        if (!empty($this->customer->address))
            $data[] = $this->customer->address;
        if (!empty($this->customer->zip))
            $data[] = $this->customer->zip;
        if (!empty($this->customer->city))
            $data[] = $this->customer->city;

        return join(', ', $data);
    }

    public function getReferenceNameAttribute()
    {
        if (is_null($this->contact)) {
            return $this->customer->name;
        }
        else {
            return $this->contact->full_name;
        }
    }

    public function getReferenceTaxcodeAttribute()
    {
        if (is_null($this->contact)) {
            return $this->customer->taxcode;
        }
        else {
            return $this->contact->taxcode;
        }
    }

    public function getReferenceCityAttribute()
    {
        if (is_null($this->contact)) {
            return $this->customer->city;
        }
        else {
            return $this->contact->city;
        }
    }

    public function getPhoneAttribute()
    {
        if (is_null($this->contact)) {
            return $this->customer->phone;
        }
        else {
            return $this->contact->phone;
        }
    }

    public function testOverlaps()
    {
        $ret = new Collection();

        foreach($this->bookings as $booking) {
            foreach($booking->slots()->where('abolished', false)->get() as $slot) {
                $test = $slot->getOverlapping();
                if (!is_null($test)) {
                    $ret->push($test);
                }
            }
        }

        return $ret;
    }

    /************************************************************ SluggableID */

    public function getSluggableID()
    {
        return sprintf('%s %s', $this->customer->name, $this->year);
    }
}
