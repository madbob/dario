## Dario

**Dario** è una applicazione per gestire spazi e le relative prenotazioni.

# Installazione

```
git clone https://gitlab.com/madbob/dario.git
cd dario
composer install
cp .env.example .env
(editare .env con i propri parametri di accesso al database)
php artisan migrate
php artisan db:seed
php artisan key:generate
```

Le credenziali di default sono username: admin@dario.it, password: admin

# Personalizzazione

È possibile aggiungere/modificare i seguenti files per personalizzare la propria istanza:

 * `public/images/logo.png`: logo che appare in homepage
 * `resources/views/documents/generic.blade.php`: template base per la generazione dei documenti, usato se non viene trovato un template specifico
 * `resources/views/documents/*.blade.php`: template specifici per le tipologie di assegnazioni previste, ogni file deve avere come nome l'identificativo della tipologia stessa

# Credits

**Dario** è stato inizialmente sviluppato per conto di Cascina Roccafranca, spazio pubblico del Comune di Torino.

Grazie a:

* Pier Carlo Devoti - progettazione e testing

# Licenza

**Dario** è distribuito in licenza AGPLv3.

Copyright (C) 2018 Roberto Guido <info@madbob.org>
