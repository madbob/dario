<div class="field">
    <label>Descrizione</label>
    <p>{{ $grant->description }}</p>
</div>

<div class="four fields">
    <div class="field">
        <label>Anno Sociale</label>
        <p>{{ $grant->yearobj->name }}</p>
    </div>

    <div class="field">
        <label>Numero Protocollo</label>
        <p>{{ $grant->number ?: 'Nessuno' }}</p>
    </div>

    <div class="field ui calendar">
        <label>Data Protocollo</label>
        <p>{{ $grant->protocol ? writeDate($grant->protocol) : 'Nessuna' }}</p>
    </div>

    <div class="field ui calendar">
        <label>Data Firma</label>
        <p>{{ $grant->signature ? writeDate($grant->signature) : 'Nessuna' }}</p>
    </div>
</div>

<div class="field">
    <label>Note</label>
    <p>{{ $grant->notes ?: 'Nessuna' }}</p>
</div>

<input type="hidden" name="grant_id" value="{{ $grant->id }}">
<input type="hidden" name="grant_type" value="{{ $grant->type }}">
<input type="hidden" name="contact_id" value="{{ $grant->contact_id }}">
<input type="hidden" name="description" value="{{ $grant->description }}">
<input type="hidden" name="grant_year" value="{{ $grant->year }}">
<input type="hidden" name="grant_number" value="{{ $grant->number }}">
<input type="hidden" name="grant_protocol" value="{{ $grant->protocol ? writeDate($grant->protocol) : '' }}">
<input type="hidden" name="grant_signature" value="{{ $grant->signature ? writeDate($grant->signature) : '' }}">
<input type="hidden" name="grant_notes" value="{{ $grant->notes }}">
