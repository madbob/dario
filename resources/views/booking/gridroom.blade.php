<?php

$column_num = $rooms->count() + 1;

$filled = [];
foreach($days as $day) {
    $d_index = $day->format('Y-m-d');
    $filled[$d_index] = [];

    for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5) {
        $filled[$d_index][(string) $i] = false;
    }
}

?>

@for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
    <tr>
        <td data-hour="{{ $i }}">{{ $t = floatToTime($i) }}</td>

        @foreach($days as $day)
            <?php $d_index = $day->format('Y-m-d') ?>

            @if($filled[$d_index][(string) $i])
                @continue
            @endif

            @if($slot = App\Slot::where('room_id', $room->id)->where('date', $day)->where('from', $t)->where('abolished', false)->first())
                @php
                    $duration = $slot->duration_cells;
                    for($void = $i; $void < ($i + ($duration / 2)); $void += 0.5)
                        $filled[$d_index][(string) $void] = true;
                @endphp

                @include('slot.cell', ['slot' => $slot])
            @else
                <td data-day="{{ $day->format('Y-m-d') }}">&nbsp;</td>
            @endif
        @endforeach
    </tr>
@endfor
