<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('grant_id');
            $table->enum('type', ['recurring', 'occasional']);
            $table->string('description')->default('');
            $table->string('room_id');
            $table->date('start');
            $table->date('end');
            $table->string('color');
            $table->text('recursion');
            $table->enum('payment', ['pay', 'free', 'contribute']);
            $table->float('unit_price', 6, 2);
            $table->float('total_price', 6, 2);
            $table->float('deposit', 6, 2)->default(0);
            $table->enum('deposit_status', ['to_pay', 'payed', 'returned']);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('grant_id')->references('id')->on('grants')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('room_id')->references('id')->on('rooms')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
