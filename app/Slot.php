<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Events\SluggableCreating;

class Slot extends Model
{
    use SluggableID;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $dispatchesEvents = [
        'creating' => SluggableCreating::class,
    ];

    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }

    public function room()
    {
        return $this->belongsTo('App\Room');
    }

    public function getGridClassAttribute()
    {
        return sprintf('event-%s %s', $this->booking->type, $this->booking->color);
    }

    public function getDurationCellsAttribute()
    {
        return ((timeToFloat($this->to) - timeToFloat($this->from)) * 2);
    }

    public static function getStaticOverlapping($booking_id, $room_id, $date, $time_start, $time_end)
    {
        return Slot::where('booking_id', '!=', $booking_id)->where('abolished', false)->where('room_id', $room_id)->where('date', $date)->where(function($query) use ($time_start, $time_end) {
            $query->where(function($subquery) use ($time_start, $time_end) {
                $subquery->where('from', '>=', $time_start)->where('from', '<', $time_end);
            })->orWhere(function($subquery) use ($time_start, $time_end) {
                $subquery->where('to', '>', $time_start)->where('to', '<=', $time_end);
            });
        })->first();
    }

    public function getOverlapping()
    {
        return self::getStaticOverlapping($this->booking_id, $this->room_id, $this->date, $this->from, $this->to);
    }

    /************************************************************ SluggableID */

    public function getSluggableID()
    {
        return sprintf('%s %s %s %s %s', $this->booking->name, $this->room->name, $this->date, $this->from, $this->to);
    }
}
