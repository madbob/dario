<div class="ui list">
    <?php $selected_grant = false ?>

    @if($customer->grants()->count() != 0)
        <div class="ui horizontal divider">Assegnazioni Esistenti</div>

        @foreach($customer->grants as $grant)
            <div class="item">
                <div class="right floated content">
                    <div class="inline field">
                        <div class="ui toggle checkbox">
                            <input type="radio" name="grant_id" value="{{ $grant->id }}" class="hidden propagate_change" {{ $selected_grant == false ? 'checked' : '' }}>
                        </div>
                    </div>
                </div>
                <div class="header">{{ $grant->description }}</div>
                {{ $grant->yearobj->name }}
            </div>

            @if($selected_grant == false)
                <?php $selected_grant = true ?>
            @endif
        @endforeach
    @endif

    <div class="ui horizontal divider">Nuova Assegnazione</div>

    <div class="item">
        <div class="right floated content">
            <div class="inline field">
                <div class="ui toggle checkbox displaying" data-displaying-target="#new_grant">
                    <input type="radio" name="grant_id" value="new" class="hidden" {{ $selected_grant == false ? 'checked' : '' }}>
                </div>
            </div>
        </div>
        <div class="header">Crea Nuova Assegnazione</div>

        <div id="new_grant" class="{{ $selected_grant == false ? '' : 'hidden' }}">
            @include('grant.embed', ['grant' => null, 'customer' => $customer])
        </div>
    </div>
</div>
