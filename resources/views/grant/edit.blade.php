<div class="ui modal">
    <i class="close icon"></i>
    <div class="header">
        @if($grant)
            Modifica Assegnazione
        @else
            Crea Assegnazione
        @endif
    </div>
    <div class="scrolling content">
        @if($grant)
            @include('grant.overlaps', ['grant' => $grant])
        @endif

        <form method="POST" action="{{ $grant ? route('grant.update', $grant->id) : route('grant.store') }}" class="ui form async-form" data-fill-dynamic-table="#grants">
            @csrf

            <div class="field" id="new_grant_wrapper">
                @if($grant)
                    <input type="hidden" name="customer_id" value="{{ $grant->customer_id }}">
                    @method('PUT')
                @else
                    @if($customer)
                        <input type="hidden" name="customer_id" value="{{ $customer->id }}">
                    @else
                        <div class="required field">
                            <label>Cliente</label>
                            @include('customer.search', ['can_create_on' => 'new_grant_wrapper'])
                        </div>
                    @endif

                    <input type="hidden" name="after_success" value="createBooking">
                @endif

                <div class="two fields">
                    <div class="required field">
                        <label>Descrizione Assegnazione</label>
                        <input type="text" name="grant_description" value="{{ $grant ? $grant->description : '' }}" required>
                    </div>

                    <div class="required field">
                        <label>Tipo</label>
                        <div class="inline fields">
                            @foreach(App\Grant::types() as $identifier => $info)
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="grant_type" value="{{ $identifier }}" class="hidden" required {{ $grant && $grant->type == $identifier ? 'checked' : '' }}>
                                        <label>{{ $info->label }}</label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                <div class="four fields">
                    <div class="{{ $grant ? '' : 'required' }} field">
                        <label>Anno Sociale</label>
                        @if($grant)
                            <p>
                                <input type="hidden" name="grant_year" value="{{ $grant->year }}">
                                {{ $grant->yearobj->name }}
                            </p>
                        @else
                            <?php $current_year = currentSocialYear() ?>
                            <select name="grant_year">
                                @foreach(App\Year::orderBy('name', 'desc')->get() as $year)
                                    <option value="{{ $year->id }}" {{ $year->id == $current_year->id ? 'selected' : '' }}>{{ $year->name }}</option>
                                @endforeach
                            </select>
                        @endif
                    </div>

                    <div class="field">
                        <label>Numero Protocollo</label>
                        <input type="number" name="grant_number" value="{{ $grant ? $grant->number : App\Grant::max('number') + 1 }}">
                    </div>

                    <div class="field ui calendar">
                        <label>Data Protocollo</label>
                        <input type="text" name="grant_protocol" value="{{ $grant ? $grant->protocol : date('Y-m-d') }}">
                    </div>

                    <div class="field ui calendar">
                        <label>Data Firma</label>
                        <input type="text" name="grant_signature" value="{{ $grant ? $grant->signature : '' }}">
                    </div>
                </div>

                <div class="field customer_contacts">
                    @if($customer)
                        @include('customer.contactselect', ['customer' => $customer, 'selected_id' => $grant ? $grant->contact_id : 0])
                    @else
                        <label>Contatto Primario</label>
                        <div class="ui message">
                            Seleziona un cliente per avere qui l'elenco dei suoi contatti.
                        </div>
                    @endif
                </div>

                <div class="field">
                    <label>Note</label>
                    <textarea name="grant_notes" rows="3">{{ $grant ? $grant->notes : '' }}</textarea>
                </div>

                @if($grant)
                    <h4 class="ui horizontal divider header">
                        <i class="calendar icon"></i>
                        Prenotazioni
                    </h4>

                    <input type="hidden" name="booking_id[]" value="-1">

                    <table id="rooms" class="ui basic table dynamic-table">
                        <thead>
                            <tr>
                                <th width="21%">Descrizione</th>
                                <th width="21%">Sala</th>
                                <th width="21%">Dal Giorno</th>
                                <th width="21%">Al Giorno</th>
                                <th width="16%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($grant->bookings as $booking)
                                <tr>
                                    <td>
                                        {{ $booking->description }}
                                    </td>
                                    <td>
                                        <input type="hidden" name="booking_id[]" value="{{ $booking->id }}">
                                        {{ $booking->room->name }}
                                    </td>
                                    <td>
                                        {{ fullDate($booking->start) }}
                                    </td>
                                    <td>
                                        {{ fullDate($booking->end) }}
                                    </td>
                                    <td>
                                        <span class="pull-right">
                                            <button class="ui blue icon button async-modal" data-modal-url="{{ route('booking.edit', $booking->id) }}"><i class="zoom in icon"></i></button>
                                            <button class="ui red icon button remove-row"><i class="trash icon"></i></button>
                                        </span>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">
                                    <button class="ui orange basic button async-modal" data-modal-url="{{ route('booking.create', ['grant_id' => $grant->id]) }}">Aggiungi Prenotazione</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                @else
                    <div class="ui message">
                        Salva l'assegnazione prima di creare nuove prenotazioni correlate.
                    </div>
                @endif

                @if($grant && $grant->payments->count() != 0)
                    <h4 class="ui horizontal divider header">
                        <i class="euro icon"></i>
                        Pagamenti
                    </h4>

                    <table id="payments" class="ui basic table">
                        <thead>
                            <tr>
                                <th width="40%">Descrizione</th>
                                <th width="20%">Prezzo</th>
                                <th width="20%">Scadenza</th>
                                <th width="10%">Pagato</th>
                                <th width="10%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($grant->payments as $payment)
                                @include('booking.payments.payment', ['payment' => $payment])
                            @endforeach

                            @foreach($grant->bookings as $booking)
                                @if($booking->deposit != 0)
                                    <tr>
                                        <td>
                                            <input type="hidden" name="deposit_id[]" value="{{ $booking->id }}">
                                            Cauzione
                                        </td>
                                        <td>
                                            <div class="ui right labeled fluid input">
                                                <input type="number" name="deposit_amount[]" value="{{ $booking->deposit }}" required>
                                                <div class="ui basic label">€</div>
                                            </div>
                                        </td>
                                        <td colspan="3">
                                            <select name="deposit_status[]">
                                                <option value="to_pay" {{ $booking->deposit_status == 'to_pay' ? 'selected' : '' }}>Da Versare</option>
                                                <option value="payed" {{ $booking->deposit_status == 'payed' ? 'selected' : '' }}>Versata</option>
                                                <option value="returned" {{ $booking->deposit_status == 'returned' ? 'selected' : '' }}>Restituita</option>
                                            </select>
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                @endif
            </div>

            <button class="ui positive button" type="submit">Salva</button>
            @if($grant)
                <a class="ui green basic button" href="{{ route('grant.document', $grant->id) }}">Scarica Documento</a>
            @endif
        </form>
    </div>
</div>
