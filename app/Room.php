<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Events\SluggableCreating;

use App\Slot;

class Room extends Model
{
    use SluggableID;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $dispatchesEvents = [
        'creating' => SluggableCreating::class,
    ];

    public function types()
    {
        return $this->belongsToMany('App\RoomType')->orderBy('name', 'asc');
    }

    public function equipments()
    {
        return $this->belongsToMany('App\Equipment');
    }

    public function getPrintableSpecialNotesAttribute()
    {
        if (!empty($this->special_notes)) {
            return sprintf('(%s)', $this->special_notes);
        }
        else {
            return '';
        }
    }

    public function getNameWithTypesAttribute()
    {
        $types = $this->types->pluck('name')->toArray();

        if (empty($types))
            return $this->name;
        else
            return sprintf('%s (%s)', $this->name, join(', ', $types));
    }

    public function hasType($id)
    {
        return ($this->types()->where('room_types.id', $id)->count() != 0);
    }

    public function hasEquipment($id)
    {
        return ($this->equipments()->where('equipment.id', $id)->count() != 0);
    }

    public function getSlot($day, $hour)
    {
        return Slot::where('room_id', $this->id)->where('date', $day)->where('from', $hour)->first();
    }
}
