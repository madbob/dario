<?php

if (!isset($selected_id))
    $selected_id = 0;

?>

@if($customer->contacts->count() != 0)
    <div class="field">
        <label>Contatto Primario</label>
        <div class="grouped fields">
            @foreach($customer->contacts as $index => $contact)
                <?php

                $checked = ($selected_id == $contact->id || ($selected_id == 0 && $index == 0));
                $actual_contacts = [];
                if (!empty($contact->phone))
                    $actual_contacts[] = $contact->phone;
                if (!empty($contact->mobile))
                    $actual_contacts[] = $contact->mobile;
                if (!empty($contact->email))
                    $actual_contacts[] = $contact->email;
                if (!empty($contact->email2))
                    $actual_contacts[] = $contact->email2;
                if (empty($actual_contacts))
                    $actual_contacts[] = 'Nessun Contatto!';

                ?>

                <div class="field">
                    <div class="ui {{ $checked ? 'checked' : '' }} toggle checkbox explicit">
                        <input type="radio" name="contact_id" tabindex="0" class="hidden" {{ $checked ? 'checked' : '' }} value="{{ $contact->id }}">
                        <label>{{ $contact->full_name }} ({{ join(', ', $actual_contacts) }})</label>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
