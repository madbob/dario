<?php

$column_size = 92 / 7;

$plain_days = [];
foreach($days as $day) {
	$plain_days[] = $day->format('Y-m-d');
}

$holidays = [];
foreach(App\Holiday::whereIn('date', $plain_days)->get() as $holiday) {
	$holidays[$holiday->date] = true;
}

?>

<tr>
	<th width="8%">
		@if($column_select)
			<div class="ui icon top left pointing dropdown button no-print" id="day-column-selector">
				<i class="wrench icon"></i>
				<div class="menu">
					@foreach(days() as $index => $day)
						<div class="item">
							<div class="ui toggle checkbox">
								<input type="checkbox" value="{{ $index }}">
								<label>{{ $day }}</label>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		@endif
	</th>

	@foreach($days as $day)
		<?php $d_index = $day->format('Y-m-d') ?>

		<th class="{{ isset($holidays[$d_index]) != null ? 'holiday' : '' }}" width="{{ $column_size }}%" data-columner-target="{{ $day->format('w') }}">
			<a href="{{ route('home', ['date' => $d_index]) }}">{{ fullDate($day, false) }}</a>
		</th>
	@endforeach
</tr>
