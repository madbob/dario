<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\JsonResource;

class Payment extends JsonResource
{
    public function toArray($request)
    {
        Resource::withoutWrapping();

        return [
            'name' => $this->printable_full_description,
        ];
    }
}
