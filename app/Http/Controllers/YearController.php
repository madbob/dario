<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Year;
use App\Holiday;
use App\Grant;

class YearController extends Controller
{
    private function handleHolidays($year, $request)
    {
        $holiday_names = $request->input('holiday_name');
        $holiday_dates = $request->input('holiday_date');
        $saved_holidays = [];

        foreach($holiday_names as $index => $name) {
            $name = trim($name);
            if (empty($name))
                continue;

            $date = readDate($holiday_dates[$index]);
            if (empty($date))
                continue;

            $holiday = Holiday::where('date', $date)->where('year', $year->id)->first();
            if ($holiday == null) {
                $holiday = new Holiday();
            }

            $holiday->name = $name;
            $holiday->date = $date;
            $holiday->year = $year->id;
            $holiday->save();

            $saved_holidays[] = $holiday->id;
        }

        Holiday::where('year', $year->id)->whereNotIn('id', $saved_holidays)->delete();
    }

    public function create()
    {
        $year = date('Y');
        $prev_year = null;

        while(true) {
            $string_year = sprintf('%s/%s', $year, $year + 1);
            $y = Year::where('name', $string_year)->first();
            if (is_null($y))
                break;

            $prev_year = $y;
            $year += 1;
        }

        if (is_null($prev_year)) {
            $year = date('Y');
            $prev_string_year = sprintf('%s/%s', $year - 1, $year);
            $prev_year = Year::where('name', $prev_string_year)->first();
            if (is_null($prev_year)) {
                $prev_year = Year::orderBy('name')->first();
            }
        }

        $year = null;

        return view('year.edit', compact('year', 'string_year', 'prev_year'));
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        $year = new Year();
        $year->name = $request->input('year');
        $year->start = readDate($request->input('start'));
        $year->end = readDate($request->input('end'));
        $year->save();

        self::handleHolidays($year, $request);

        $grants = $request->input('grants', []);

        foreach($grants as $grant_id) {
            $old_grant = Grant::find($grant_id);
            $new_grant = $old_grant->replicate();
            $new_grant->year = $year->id;
            $new_grant->save();

            foreach($old_grant->bookings()->yearly()->get() as $booking) {
                $new_booking = $booking->replicate();
                $new_booking->grant_id = $new_grant->id;
                $new_booking->start = $year->start;
                $new_booking->end = $year->end;
                $new_booking->save();
                $new_booking->generateSlots();
            }
        }

        DB::commit();

        return redirect()->route('config.index');
    }

    public function edit($id)
    {
        $year = Year::find($id);
        return view('year.edit', compact('year'));
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        $year = Year::find($id);
        $year->start = readDate($request->input('start'));
        $year->end = readDate($request->input('end'));
        $year->save();

        self::handleHolidays($year, $request);

        DB::commit();

        return redirect()->route('config.index');
    }
}
