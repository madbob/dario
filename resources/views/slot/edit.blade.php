<div class="ui modal">
    <i class="close icon"></i>
    <div class="header">
        Modifica Slot
    </div>
    <div class="content">
        <form method="POST" action="{{ route('slot.update', $slot->id) }}" class="ui form reload-form">
            @csrf
            @method('PUT')

            <div class="field">
                <label>Sala</label>
                <select name="room_id">
                    @foreach(App\Room::orderBy('name', 'desc')->get() as $room)
                        <option value="{{ $room->id }}" {{ $slot->room_id == $room->id ? 'selected' : '' }}>{{ $room->name }}</option>
                    @endforeach
                </select>
            </div>

            <div class="field ui calendar">
                <label>Giorno</label>
                <input type="text" name="date" value="{{ $slot->date }}">
            </div>

            <div class="two fields">
                <div class="field ui calendar">
                    <label>Dalle Ore</label>
                    <select name="from">
                        @for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
                            <?php $t = floatToTime($i) ?>
                            <option value="{{ $i }}" {{ reducedTime($slot->from) == $t ? 'selected' : '' }}>{{ $t }}</option>
                        @endfor
                    </select>
                </div>
                <div class="field ui calendar">
                    <label>Alle Ore</label>
                    <select name="to">
                        @for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
                            <?php $t = floatToTime($i) ?>
                            <option value="{{ $i }}" {{ reducedTime($slot->to) == $t ? 'selected' : '' }}>{{ $t }}</option>
                        @endfor
                    </select>
                </div>
            </div>

            <div class="ui message">
                <p>
                    {{ $slot->booking->grant->description }} - assegnazione {{ $slot->booking->grant->number != 0 ? $slot->booking->grant->number : '' }} {{ $slot->booking->grant->protocol != null ? 'del ' . writeDate($slot->booking->grant->protocol) : '' }} a {{ $slot->booking->grant->customer->name }}
                </p>

                <p>
                    <a class="ui blue label async-modal" data-modal-url="{{ route('grant.edit', $slot->booking->grant->id) }}">
                        <i class="cog icon"></i>
                        Vedi Assegnazione
                    </a>

                    <a class="ui blue label async-modal" data-modal-url="{{ route('booking.edit', $slot->booking->id) }}">
                        <i class="cog icon"></i>
                        Vedi Prenotazione Complessiva
                    </a>
                </p>
            </div>

            <button class="ui positive button" type="submit">Salva</button>
            <a class="ui green basic button" href="{{ route('grant.document', $slot->booking->grant_id) }}">Scarica Documento</a>
            <a class="ui negative button right floated async-modal" href="#" data-modal-url="{{ route('slot.askdestroy', $slot->id) }}">Elimina</a>
        </form>
    </div>
</div>
