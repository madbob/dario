<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Events\SluggableCreating;

class Contact extends Model
{
    use SluggableID;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $dispatchesEvents = [
        'creating' => SluggableCreating::class,
    ];

    public function customers()
    {
        return $this->belongsToMany('App\Customer');
    }

    public function getFullNameAttribute()
    {
        $data = [];

        if (!empty($this->name))
            $data[] = $this->name;
        if (!empty($this->surname))
            $data[] = $this->surname;

        return join(' ', $data);
    }

    /************************************************************ SluggableID */

    public function getSluggableID()
    {
        return $this->full_name;
    }
}
