<tr class="{{ $visible ? '' : 'reference-row' }}">
    <td>
        <input type="hidden" name="choosen_day_id[]" value="{{ $visible ? 'new' : 'do_not_use' }}">
        <div class="field ui calendar" id="{{ $visible ? 'choose_day_' . ceil(rand() * 100) : '' }}">
            <input type="text" name="choosen_days[]" value="{{ $day ? $day : date('Y-m-d') }}" required>
        </div>
    </td>
    <td>
        <select name="from[]">
            @for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
                <?php $t = floatToTime($i) ?>
                <option value="{{ $i }}" {{ floatToTime($from) == $t ? 'selected' : '' }}>{{ $t }}</option>
            @endfor
        </select>
    </td>
    <td>
        <select name="to[]">
            @for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
                <?php $t = floatToTime($i) ?>
                <option value="{{ $i }}" {{ floatToTime($to) == $t ? 'selected' : '' }}>{{ $t }}</option>
            @endfor
        </select>
    </td>
    <td>
        <button class="ui red icon button remove-row"><i class="trash icon"></i></button>
    </td>
</tr>
