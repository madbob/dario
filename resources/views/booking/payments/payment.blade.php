<?php

$days = 0;
if (isset($payment->involved_days))
    $days = $payment->involved_days;
else
    $days = $payment->quarter_days;

if (!isset($has_days))
    $has_days = false;

?>

<tr {!! $days != 0 ? 'data-involved_days="' . $days . '"' : '' !!}>
    <td>
        <input type="hidden" name="payment_id[]" value="{{ $payment ? $payment->id : 'empty' }}">
        <input type="hidden" name="payment_quarter[]" value="{{ $payment ? $payment->quarter : 'none' }}">
        <div class="required field">
            <input type="text" name="payment_description[]" value="{{ $payment ? $payment->description : '' }}" required>
        </div>
    </td>
    @if($has_days)
        <td>
            {{ $days }} giorni
        </td>
    @endif
    <td>
        <div class="ui right labeled fluid input">
            <input type="number" step="0.01" name="payment_amount[]" value="{{ $payment ? $payment->amount : '' }}" required>
            <div class="ui basic label">€</div>
        </div>
    </td>
    <td>
        <div class="field ui calendar">
            <input type="text" name="payment_date[]" value="{{ $payment ? widgetDate($payment->date) : date('Y-m-d') }}" required>
        </div>
    </td>
    <td>
        <div class="ui toggle checkbox explicit {{ $payment && $payment->payed ? 'checked' : '' }}">
            <input type="checkbox" name="{{ \Illuminate\Support\Str::random(10) }}" tabindex="0" class="hidden" {{ $payment && $payment->payed ? 'checked' : '' }}>
            <input type="hidden" name="payment_payed[]" value="{{ $payment && $payment->payed ? '1' : '0' }}">
        </div>
    </td>
    <td>
        <button class="ui red icon button pull-right remove-row"><i class="trash icon"></i></button>
    </td>
</tr>
