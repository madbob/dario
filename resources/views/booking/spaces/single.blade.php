<?php

if (empty($selected_day))
    $selected_day = date('Y-m-d');
if (!isset($selected_end_day))
    $selected_end_day = $selected_day;

?>

<div>
    <div class="required field">
        <label>Sala</label>
        <select class="select-loader" data-load-area=".equipment-select" name="room_id" required>
            @foreach($currentuser->rooms()->orderBy('name', 'asc')->get() as $room)
                <option value="{{ $room->id }}" {{ $selected_room == $room->id ? 'selected' : '' }} data-load-url="{{ route('room.equipment', $room->id) }}">{{ $room->name_with_types }}</option>
            @endforeach
        </select>
    </div>

    @include('room.equipmentselect', ['room' => $selected_room, 'selected' => $selected_equipment])

    <div class="four fields">
        <div class="required field ui calendar" data-change-function="verifyEndDate" data-end-widget="#endDate">
            <label>Dal Giorno</label>
            <input type="text" name="start_day" value="{{ $selected_day ? $selected_day : date('Y-m-d') }}" required>
        </div>

        <div class="required field ui calendar" id="endDate">
            <label>Al Giorno</label>
            <input type="text" name="end_day" value="{{ $selected_end_day ? $selected_end_day : date('Y-m-d') }}" required>
        </div>

        <div class="required field">
            <label>Dalle Ore</label>
            <select name="from" required>
                @for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
                    <?php $t = floatToTime($i) ?>
                    <option value="{{ $i }}" {{ $selected_start_hour == $t ? 'selected' : '' }}>{{ $t }}</option>
                @endfor
            </select>
        </div>

        <div class="required field">
            <label>Alle Ore</label>
            <select name="to" required>
                @for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
                    <?php $t = floatToTime($i) ?>
                    <option value="{{ $i }}" {{ $selected_end_hour == $t ? 'selected' : '' }}>{{ $t }}</option>
                @endfor
            </select>
        </div>
    </div>

    <div class="field">
        <a class="ui basic button test-booking-days async-portion" href="#" data-portion-url="{{ route('booking.wizard', ['page' => 'test']) }}" data-container-id="days_tester">Verifica Giorni</a>
        <div id="days_tester"></div>
    </div>
</div>
