<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

use Log;

use App\Events\SluggableCreating;

use App\Slot;
use App\Holiday;

class Booking extends Model
{
    use SluggableID;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $dispatchesEvents = [
        'creating' => SluggableCreating::class,
    ];

    public function room()
    {
        return $this->belongsTo('App\Room');
    }

    public function equipments()
    {
        return $this->belongsToMany('App\Equipment');
    }

    public function grant()
    {
        return $this->belongsTo('App\Grant');
    }

    public function slots()
    {
        return $this->hasMany('App\Slot');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public function getPrintableDescriptionAttribute()
    {
        if ($this->grant->bookings()->count() > 1)
            return $this->description;
        else
            return $this->grant->description;
    }

    public function getMacroTypeAttribute()
    {
        switch($this->type) {
            case 'recurring':
                return 'recurring';
                break;

            case 'occasional':
                return 'single';
                break;

            case 'manual':
                return 'manual';
                break;

            default:
                Log::error('Prenotazione di tipo non identificato: ' . $this->type);
                return '';
                break;
        }
    }

    public function getRecursionObjAttribute()
    {
        return json_decode($this->recursion);
    }

    public function getExcludedDaysAttribute()
    {
        return ($this->recursion_obj->excluded ?? []);
    }

    public function getHoursAttribute()
    {
        $hours = 0;

        foreach($this->slots as $slot) {
            $hours += timeToFloat($slot->to) - timeToFloat($slot->from);
        }

        return $hours;
    }

    public function scopeYearly($query)
    {
        $query->where('recursion', 'like', '%yearly%');
    }

    /*
        $days si assume essere l'output di staticGetDays() e affini
    */
    public static function sortDaysByQuarter($days)
    {
        $quarters = (object) [
            'first' => (object) [
                'year' => 0,
                'days' => 0
            ],
            'second' => (object) [
                'year' => 0,
                'days' => 0
            ],
            'third' => (object) [
                'year' => 0,
                'days' => 0
            ],
            'fourth' => (object) [
                'year' => 0,
                'days' => 0
            ],
        ];

        foreach($days as $date => $d) {
            if (!is_null($d->holiday))
                continue;

            list($year, $month, $day) = explode('-', $date);

            if ($month <= 3)
                $q = 'first';
            else if ($month <= 6)
                $q = 'second';
            else if ($month <= 9)
                $q = 'third';
            else
                $q = 'fourth';

            $quarters->$q->days += 1;
            $quarters->$q->year = $year;
        }

        return $quarters;
    }

    public static function staticGetDays($start, $end, $info, $include_holidays = true)
    {
        if (is_string($start))
            $start = new \DateTime($start);
        if (is_string($end))
            $end = new \DateTime($end);

        $days = [];
        $end = $end->modify('+1 days');
        $all_days = new \DatePeriod($start, new \DateInterval('P1D'), $end);
        $excluded_days = $info->excluded ?? [];

        foreach($info->cycle as $cycle) {
            $week_offset = 1;
            $validity_start = 1;
            $validity_end = 31;

            switch($cycle) {
                case 'all':
                    break;
                case 'first':
                    $validity_start = 1;
                    $validity_end = 7;
                    break;
                case 'second':
                    $validity_start = 8;
                    $validity_end = 14;
                    break;
                case 'third':
                    $validity_start = 15;
                    $validity_end = 21;
                    break;
                case 'fourth':
                    $validity_start = 22;
                    $validity_end = 28;
                    break;
                case 'last':
                    $validity_start = 25;
                    $validity_end = 31;
                    break;
                case '2_weeks':
                    $week_offset = 2;
                    break;
                case '3_weeks':
                    $week_offset = 3;
                    break;
                case '4_weeks':
                    $week_offset = 4;
                    break;
                default:
                    Log::error('Tipo ciclicità non identificato: ' . $cycle);
                    break;
            }

            $week_index = 0;

            foreach($all_days as $d) {
                $week_index++;
                if ($week_offset != 1 && $week_index % $week_offset == 0)
                    continue;

                $d_day = $d->format('d');
                if ($d_day < $validity_start || $d_day > $validity_end)
                    continue;

                foreach($info->days as $info_d) {
                    if ($d->format('w') == $info_d->offset) {
                        $fulldate = $d->format('Y-m-d');

                        if (in_array($fulldate, $excluded_days)) {
                            continue;
                        }

                        $days[$fulldate] = (object)[
                            'from' => $info_d->from,
                            'to' => $info_d->to,
                            'holiday' => null
                        ];
                        break;
                    }
                }
            }

            $all_dates = array_keys($days);
            $holidays = Holiday::whereIn('date', $all_dates)->get();
            foreach($holidays as $h) {
                if ($include_holidays == false) {
                    unset($days[$h->date]);
                }
                else {
                    $days[$h->date]->holiday = $h;
                }
            }
        }

        ksort($days);
        return $days;
    }

    public static function staticGetManualDays($days, $starts, $ends, $include_holidays)
    {
        $ret = [];

        foreach($days as $index => $date) {
            $holiday = Holiday::where('date', $date)->first();
            if ($include_holidays == false && $holiday != null) {
                continue;
            }

            $ret[$date] = (object)[
                'from' => $starts[$index],
                'to' => $ends[$index],
                'holiday' => $holiday
            ];
        }

        return $ret;
    }

    public function getDays($include_holidays = true)
    {
        if ($this->type == 'manual') {
            $days = array_map(function($i) {
                return $i->date;
            }, $this->recursion_obj->days);

            $starts = array_map(function($i) {
                return $i->from;
            }, $this->recursion_obj->days);

            $ends = array_map(function($i) {
                return $i->to;
            }, $this->recursion_obj->days);

            return self::staticGetManualDays($days, $starts, $ends, $include_holidays);
        }
        else {
            return self::staticGetDays($this->start, $this->end, $this->recursion_obj, $include_holidays);
        }
    }

    public function generateSlots($request = null)
    {
        $saved_slots = [];
        $days = $this->getDays(false);

        if ($request) {
            $to_restore = $request->input('to_restore', []);
        }
        else {
            $to_restore = [];
        }

        foreach($days as $d => $details) {
            $time_start = floatToTime($details->from);
            $time_end = floatToTime($details->to);

            $slot = $this->slots()->where('room_id', $this->room_id)->where('date', $d)->where('from', $time_start)->where('to', $time_end)->first();
            if (is_null($slot)) {
                $slot = new Slot();
                $slot->booking_id = $this->id;
                $slot->room_id = $this->room_id;
                $slot->date = $d;
                $slot->from = $time_start;
                $slot->to = $time_end;
                $slot->save();
            }
            else {
                if ($slot->abolished && in_array($slot->id, $to_restore)) {
                    $slot->abolished = false;
                    $slot->save();
                }
            }

            $existing = $slot->getOverlapping();
            if($existing) {
                $action = $request->input('keepers_' . $existing->id);
                if ($action == 'keep') {
                    $slot->abolished = true;
                    $existing->abolished = false;
                }
                else if ($action == 'replace') {
                    $slot->abolished = false;
                    $existing->abolished = true;
                }
                else {
                    Log::error('Unable to determine conflicting status between slots');
                    throw new \Exception("Conflitto non risolto con prenotazione già esistente", 1);
                }

                $slot->save();
                $existing->save();
            }

            $saved_slots[] = $slot->id;
        }

        $this->slots()->whereNotIn('id', $saved_slots)->delete();
    }

    public function getYearHumanSummaryAttribute()
    {
        $days_names = days();
        $info = $this->recursion_obj;

        $ret = [];

        foreach($info->cycle as $cycle) {
            $cycle_prefix = '';

            switch($cycle) {
                case 'all':
                    $cycle_prefix = 'Tutti i';
                    break;
                case 'first':
                    $cycle_prefix = 'Primo';
                    break;
                case 'second':
                    $cycle_prefix = 'Secondo';
                    break;
                case 'third':
                    $cycle_prefix = 'Terzo';
                    break;
                case 'fourth':
                    $cycle_prefix = 'Quarto';
                    break;
                case 'last':
                    $cycle_prefix = 'Ultimo';
                    break;
                case '2_weeks':
                    $cycle_prefix = 'Ogni 2';
                    break;
                case '3_weeks':
                    $cycle_prefix = 'Ogni 3';
                    break;
                case '4_weeks':
                    $cycle_prefix = 'Ogni 4';
                    break;
                default:
                    Log::error('Tipo ciclicità non identificato: ' . $cycle);
                    break;
            }

            foreach($info->days as $info_d) {
                $row = sprintf('%s - %s %s - %s / %s', $this->room->name, $cycle_prefix, $days_names[$info_d->offset], floatToTime($info_d->from), floatToTime($info_d->to));
            }

            $ret[] = $row;
        }

        return join("\n", $ret);
    }

    public function printableDates()
    {
        $days = $this->getDays(false);
        $printable_days = [];
        $printable_hours = [];

        foreach($days as $day => $data) {
            $hours = sprintf('%s / %s', floatToTime($data->from), floatToTime($data->to));
            $printable_hours[$hours] = true;
            $printable_days[] = writeDate($day);
        }

        return [
            'count_days' => count($days),
            'days' => $printable_days,
            'merged_days' => join(' - ', $printable_days),
            'hours' => join(' - ', array_keys($printable_hours))
        ];
    }

    /************************************************************ SluggableID */

    public function getSluggableID()
    {
        return sprintf('%s %s %s', $this->grant->description, $this->room->name, Str::random(5));
    }
}
