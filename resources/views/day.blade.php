@extends('layouts.app')

@section('content')
    @include('layouts.menu', ['active' => 'home', 'mode' => 'day'])

    @if($rooms->count() == 0)
        <h2 class="ui center aligned icon header">
            <i class="circular settings icon"></i>
            Per iniziare, crea almeno una sala.
        </h2>
    @else
        <?php

        $column_size = 92 / $rooms->count();
        $is_holiday = (App\Holiday::where('date', $day)->first() != null);

        ?>

        <div class="print-only">
            <h2>{{ fullDate($day) }}</h2>
        </div>

        <div class="freeze-table">
            <table class="ui selectable structured table fixed celled main-grid">
                <thead>
                    <tr>
                        <th width="8%">
                            <div class="ui icon top left pointing dropdown button no-print" id="room-column-selector">
                                <i class="wrench icon"></i>
                                <div class="menu">
                                    @foreach($rooms as $room)
                                        <div class="item">
                                            <div class="ui toggle checkbox">
                                                <input type="checkbox" value="{{ $room->id }}">
                                                <label>{{ $room->name }}</label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </th>

                        @foreach($rooms as $room)
                            <th class="{{ $is_holiday ? 'holiday' : '' }}" width="{{ $column_size }}%" data-room-id="{{ $room->id }}" data-columner-target="{{ $room->id }}">{{ $room->name }}</th>
                        @endforeach
                    </tr>
                </thead>
                <tbody>
                    @include('booking.gridday', ['day' => $day, 'show_day' => false, 'rooms' => $rooms])
                </tbody>
            </table>
        </div>
    @endif
@endsection
