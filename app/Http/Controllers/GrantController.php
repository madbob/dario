<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

use DB;
use PDF;

use App\Grant;
use App\Year;
use App\Payment;
use App\Booking;
use App\Customer;
use App\Http\Resources\Grant as GrantResource;

class GrantController extends Controller
{
    public function index(Request $request)
    {
        $current_year = $request->input('year', null);
        if ($current_year == null)
            $current_year = Year::orderBy('name', 'desc')->first()->id;

        $grants = Grant::where('year', $current_year)->orderBy('created_at', 'asc')->get();
        return view('grant.index', compact('grants', 'current_year'));
    }

    public function search($id)
    {
        $grant = Grant::where('customer_id', $id)->where('year', currentSocialYear()->id)->first();
        $customer = Customer::find($id);
        return view('grant.embed', compact('grant', 'customer'));
    }

    private function readRequest($grant, $request)
    {
        $grant->type = $request->input('grant_type');

        $customer_id = $request->input('customer_id', 'new');
        if ($customer_id == 'new') {
            $customer = new Customer();
            $customer->readRequest($request, 'customer_');
            $customer->save();
            $customer->readContacts($request, 'customer_contact_id');
            $grant->customer_id = $customer->id;
        }
        else {
            $grant->customer_id = $request->input('customer_id');
        }

        $grant->contact_id = $request->input('contact_id', 0);
        $grant->year = $request->input('grant_year');
        $grant->description = $request->input('grant_description');

        $grant->number = $request->input('grant_number');
        if (empty($grant->number))
            $grant->number = null;

        $grant->protocol = $request->input('grant_protocol');
        if (empty($grant->protocol))
            $grant->protocol = null;
        else
            $grant->protocol = readDate($grant->protocol);

        $grant->signature = $request->input('grant_signature');
        if (empty($grant->signature))
            $grant->signature = null;
        else
            $grant->signature = readDate($grant->signature);

        $grant->notes = $request->input('grant_notes', '');
    }

    public function create(Request $request)
    {
        $grant = null;
        $customer_id = $request->input('customer_id');
        $customer = Customer::find($customer_id);
        return view('grant.edit', compact('grant', 'customer'));
    }

    public function creatingByExistingCustomer()
    {
        return view('customer.search');
    }

    public function creatingByNewCustomer()
    {
        $customer = null;
        return view('customer.registry', compact('customer'));
    }

    public function store(Request $request)
    {
        $grant = new Grant();
        self::readRequest($grant, $request);
        $grant->save();
        return new GrantResource($grant);
    }

    public function edit($id)
    {
        $grant = Grant::findOrFail($id);
        $customer = $grant->customer;
        return view('grant.edit', compact('grant', 'customer'));
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        $grant = Grant::findOrFail($id);
        self::readRequest($grant, $request);

        /*
            Aggiornando una assegnazione, vengono inviati gli array sia delle
            prenotazioni che dei pagamenti assegnati alle prenotazioni stesse.
            Se rimuovo una prenotazione, rischio di manipolare poi dei pagamenti
            che non devono esistere più. Sicchè qui prima gestisco i pagamenti,
            e all'occorrenza elimino le prenotazioni di cui è stata richiesta la
            cancellazione (cancellando quegli stessi pagamenti)
        */
        Payment::saveBatch($request);

        if ($request->has('booking_id'))
            $grant->bookings()->whereNotIn('id', $request->input('booking_id'))->delete();

        $deposit_ids = $request->input('deposit_id', []);
        $deposit_amount = $request->input('deposit_amount', []);
        $deposit_statuses = $request->input('deposit_status', []);
        if (!empty($deposit_ids)) {
            foreach($deposit_ids as $index => $id) {
                $booking = Booking::find($id);
                if (is_null($booking))
                    continue;

                $booking->deposit = $deposit_amount[$index];
                $booking->deposit_status = $deposit_statuses[$index];
                $booking->save();
            }
        }

        $grant->save();

        DB::commit();

        return new GrantResource($grant);
    }

    public function document($id)
    {
        $grant = Grant::findOrFail($id);

        $template = 'documents.' . $grant->type;
        if (View::exists($template) == false)
            $template = 'documents.generic';

        $pdf = PDF::loadView($template, ['grant' => $grant]);
        $filename = sprintf('%s - %s.pdf', $grant->customer->name, $grant->number ? $grant->number : 0);
        return $pdf->download($filename);
    }

    public function askdestroy($id)
    {
        $grant = Grant::findOrFail($id);
        return view('grant.askdestroy', compact('grant'));
    }

    public function destroy($id)
    {
        $grant = Grant::findOrFail($id);
        $grant->delete();
        return redirect()->route('grant.index');
    }
}
