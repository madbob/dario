<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Events\SluggableCreating;

class User extends Authenticatable
{
    use Notifiable, SluggableID;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $dispatchesEvents = [
        'creating' => SluggableCreating::class,
    ];

    public function rooms()
    {
        return $this->belongsToMany('App\Room')->orderBy('name');
    }
}
