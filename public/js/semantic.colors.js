(function ($) {
    $.fn.colors = function() {
        var colors = ['red', 'orange', 'yellow', 'olive', 'green', 'teal', 'blue', 'violet', 'purple', 'pink', 'brown', 'grey', 'black'];

        $(this).each(function() {
            var name = $(this).attr('name');
            var value = $(this).attr('value');

            var widget = $('<div>');
            widget.addClass('ui').addClass('buttons').addClass('color-select');

            var input = $('<input>');
            input.attr('name', name).attr('type', 'hidden');
            widget.append(input);

            for(var i = 0; i < colors.length; i++) {
                var button = $('<div>');
                button.addClass('ui').addClass('button').addClass('icon').addClass(colors[i]).attr('data-selected-color', colors[i]);

                button.click(function(e) {
                    e.preventDefault();
                    widget.find('.button').empty();
                    $(this).append('<i class="check icon"></i>');
                    input.val($(this).attr('data-selected-color'));
                });

                widget.append(button);
            }

            $(this).replaceWith(widget);

            if (value == '')
                value = colors[0];

            widget.find('.' + value).click();
        });
    };
}(jQuery));
