<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Dario') }}</title>

        <link rel="stylesheet" type="text/css" href="{{ mix('css/dario.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ mix('css/print.css') }}" media="print">

        <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
        <script src="{{ asset('js/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/semantic.js') }}"></script>
        <script src="{{ asset('js/calendar.js') }}"></script>
        <script src="{{ asset('js/semantic.wizard.js') }}"></script>
        <script src="{{ asset('js/semantic.colors.js') }}"></script>
        <script src="{{ asset('js/js.cookie.js') }}"></script>
        <script src="{{ asset('js/columner.js') }}"></script>
        <script src="{{ asset('js/freeze-table.js') }}"></script>
        <script src="{{ asset('js/dario.js') }}"></script>
    </head>

    <body class="printable">
        @yield('content')
    </body>
</html>
