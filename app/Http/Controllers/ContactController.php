<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Log;

use App\Contact;
use App\Http\Resources\Contact as ContactResource;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        $display = $request->input('display', 'recent');

        if ($display == 'all') {
            $contacts = Contact::orderBy('surname', 'asc')->get();
        }
        else {
            $contacts = Contact::orderBy('surname', 'asc')->whereHas('customers', function($query) {
                $query->recent();
            })->get();
        }

        return view('contact.index', compact('contacts', 'display'));
    }

    public function search(Request $request)
    {
        $search = $request->input('search');
        $ret = (object) [
            'results' => []
        ];

        $contacts = Contact::where('name', 'like', "%$search%")->orWhere('surname', 'like', "%$search%")->get();

        foreach($contacts as $contact) {
            $ret->results[] = (object) [
                'id' => $contact->id,
                'title' => $contact->full_name,
                'name' => $contact->name,
                'surname' => $contact->surname,
                'city' => $contact->city,
                'address' => $contact->address,
                'zip' => $contact->zip,
                'taxcode' => $contact->taxcode,
                'email' => $contact->email,
                'email2' => $contact->email2,
                'phone' => $contact->phone,
                'mobile' => $contact->mobile,
                'notes' => $contact->notes,
            ];
        }

        return response()->json($ret);
    }

    private function readRequest($contact, $request)
    {
        $contact->name = $request->input('name');
        $contact->surname = $request->input('surname');
        $contact->address = $request->input('address', '');
        $contact->city = $request->input('city', '');
        $contact->zip = $request->input('zip', '');
        $contact->taxcode = $request->input('taxcode', '');
        $contact->phone = $request->input('phone', '');
        $contact->mobile = $request->input('mobile', '');
        $contact->email = $request->input('email', '');
        $contact->email2 = $request->input('email2', '');
        $contact->notes = $request->input('notes', '');
    }

    public function create(Request $request)
    {
        $contact = null;
        $mode = $request->input('mode', 'modal');

        if ($mode == 'modal') {
            return view('contact.modal_edit', compact('contact'));
        }
        else {
            $embedded = false;
            return view('contact.edit', compact('contact', 'embedded'));
        }
    }

    public function store(Request $request)
    {
        /*
            In fase di creazione di un contatto posso anche pescarne uno
            esistente, qui gestisco il caso particolare e nel caso aggiorno quel
            che c'è
        */
        $id = $request->input('id');
        $contact = Contact::find($id);
        if ($contact == null)
            $contact = new Contact();

        self::readRequest($contact, $request);
        $contact->save();

        if ($request->ajax())
            return new ContactResource($contact);
        else
            return redirect()->route('contact.index');
    }

    public function askCSV()
    {
        return view('contact.csv');
    }

    public function importCSV(Request $request)
    {
        try {
            $uploaded = $request->file('file', null);
            $filepath = sys_get_temp_dir();
            $filename = $uploaded->getClientOriginalName();
            $uploaded->move($filepath, $filename);
            $f = fopen($filepath . '/' . $filename, 'r');

            while($row = fgetcsv($f)) {
                try {
                    foreach($row as $index => $value)
                        $row[$index] = trim($row[$index]);

                    $name = $row[0] ?? '';
                    $surname = $row[1] ?? '';

                    if (empty($name) || empty($surname))
                        continue;

                    Log::debug("Importo contatto $name $surname");

                    $c = Contact::where('name', $name)->where('surname', $surname)->first();
                    if (is_null($c)) {
                        $c = new Contact();
                        $c->name = $name;
                        $c->surname = $surname;
                    }

                    $c->email = !empty($row[2]) ? $row[2] : $c->email;
                    $c->email2 = !empty($row[3]) ? $row[3] : $c->email2;
                    $c->phone = !empty($row[4]) ? $row[4] : $c->phone;
                    $c->mobile = !empty($row[5]) ? $row[5] : $c->mobile;
                    $c->taxcode = !empty($row[6]) ? $row[6] : $c->taxcode;
                    $c->address = !empty($row[7]) ? $row[7] : $c->address;
                    $c->city = !empty($row[8]) ? $row[8] : $c->city;
                    $c->zip = !empty($row[9]) ? $row[9] : $c->zip;
                    $c->notes = !empty($row[10]) ? $row[10] : ($c->notes ? $c->notes : '');

                    $c->save();
                }
                catch(\Exception $e) {
                    Log::error('Impossibile importare contatto da CSV: ' . $e->getMessage());
                }
            }
        }
        catch(\Exception $e) {
            Log::error('Impossibile importare CSV per contatti: ' . $e->getMessage());
        }

        return redirect()->route('contact.index');
    }

    public function edit(Request $request, $id)
    {
        $contact = Contact::find($id);
        $mode = $request->input('mode', 'modal');

        if ($mode == 'modal') {
            return view('contact.modal_edit', compact('contact'));
        }
        else {
            $embedded = false;
            return view('contact.edit', compact('contact', 'embedded'));
        }
    }

    public function update(Request $request, $id)
    {
        $contact = Contact::find($id);
        self::readRequest($contact, $request);

        if ($request->has('full_update'))
            $contact->customers()->sync($request->input('customer_id'));

        $contact->save();

        if ($request->ajax())
            return new ContactResource($contact);
        else
            return redirect()->route('contact.index');
    }

    public function askdestroy($id)
    {
        $contact = Contact::find($id);
        return view('contact.askdestroy', compact('contact'));
    }

    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
        return redirect()->route('contact.index');
    }
}
