<form method="POST" action="{{ $customer ? route('customer.update', $customer->id) : route('customer.store') }}" class="ui form">
    @csrf

    @if($customer)
        @method('PUT')
    @endif

    @include('customer.registry', ['customer' => $customer])

    <br>

    <div class="field contacts-table">
        <h4 class="ui horizontal divider header">
            <i class="phone icon"></i>
            Contatti
        </h4>

        <table id="contacts" class="ui basic table dynamic-table" data-max-rows="{{ $customer && $customer->type == 'person' ? 1 : 99 }}">
            <thead>
                <tr>
                    <th width="14%">Nome</th>
                    <th width="14%">Cognome</th>
                    <th width="14%">E-Mail</th>
                    <th width="14%">E-Mail Secondaria</th>
                    <th width="14%">Telefono</th>
                    <th width="14%">Cellulare</th>
                    <th width="15%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @if($customer)
                    @foreach($customer->contacts as $contact)
                        <tr>
                            <td>
                                <input type="hidden" name="contact_id[]" value="{{ $contact->id }}" class="id_into_table">
                                <span data-autocompletable-map="name">{{ $contact->name }}</span>
                            </td>
                            <td data-autocompletable-map="surname">{{ $contact->surname }}</td>
                            <td data-autocompletable-map="email">{{ $contact->email }}</td>
                            <td data-autocompletable-map="email2">{{ $contact->email2 }}</td>
                            <td data-autocompletable-map="phone">{{ $contact->phone }}</td>
                            <td data-autocompletable-map="mobile">{{ $contact->mobile }}</td>
                            <td>
                                <span class="pull-right">
                                    <button class="ui blue icon button async-modal" data-modal-url="{{ route('contact.edit', $contact->id) }}"><i class="address card icon"></i></button>
                                    <button class="ui red icon button remove-row"><i class="trash icon"></i></button>
                                </span>
                            </td>
                        </tr>
                    @endforeach
                @endif

                <tr class="reference-row">
                    <td>
                        <input type="hidden" name="contact_id[]" value="" data-autocompletable-map="id" class="id_into_table">
                        <span data-autocompletable-map="name"></span>
                    </td>
                    <td data-autocompletable-map="surname"></td>
                    <td data-autocompletable-map="email"></td>
                    <td data-autocompletable-map="email2"></td>
                    <td data-autocompletable-map="phone"></td>
                    <td data-autocompletable-map="mobile"></td>
                    <td>
                        <span class="pull-right">
                            <button class="ui blue icon button async-modal" data-autocompletable-map="edit_url" data-modal-url=""><i class="address card icon"></i></button>
                            <button class="ui red icon button remove-row"><i class="trash icon"></i></button>
                        </span>
                    </td>
                </tr>
            </tbody>
            @if($customer == null || $customer->type != 'person' || $customer->contacts->count() < 1)
                <tfoot>
                    <tr>
                        <td colspan="2">
                            <button class="ui orange basic button async-modal" data-modal-url="{{ route('contact.create') }}">Aggiungi Contatto</button>
                        </td>
                    </tr>
                </tfoot>
            @endif
        </table>
    </div>

    @if($customer)
        <h4 class="ui horizontal divider header">
            <i class="calendar icon"></i>
            Assegnazioni
        </h4>

        <table id="grants" class="ui basic table dynamic-table">
            <thead>
                <tr>
                    <th width="20%">Anno Sociale</th>
                    <th width="30%">Descrizione</th>
                    <th width="35%">Pagamenti</th>
                    <th width="15%">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                @foreach($customer->grants as $grant)
                    <tr>
                        <td>
                            <input type="hidden" name="grant_id[]" value="{{ $grant->id }}" class="id_into_table">
                            <span data-autocompletable-map="year">{{ $grant->yearobj->name }}</span>
                        </td>
                        <td data-autocompletable-map="description">{{ $grant->description }}</td>
                        <td>
                            <ul data-autocompletable-map="payments">
                                @foreach($grant->payments as $payment)
                                    <li>{!! $payment->printable_full_description !!}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>
                            <span class="pull-right">
                                <button class="ui blue icon button async-modal" data-modal-url="{{ route('grant.edit', $grant->id) }}"><i class="zoom in icon"></i></button>
                                <button class="ui red icon button remove-row"><i class="trash icon"></i></button>
                            </span>
                        </td>
                    </tr>
                @endforeach

                <tr class="reference-row">
                    <td>
                        <input type="hidden" name="grant_id[]" data-autocompletable-map="id" class="id_into_table">
                        <span data-autocompletable-map="year"></span>
                    </td>
                    <td data-autocompletable-map="description"></td>
                    <td>
                        <ul data-autocompletable-map="payments"></ul>
                    </td>
                    <td>
                        <span class="pull-right">
                            <button class="ui blue icon button async-modal" data-autocompletable-map="edit_url" data-modal-url=""><i class="zoom in icon"></i></button>
                            <button class="ui red icon button remove-row"><i class="trash icon"></i></button>
                        </span>
                    </td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4">
                        @if($customer->type != 'person' && $customer->contacts()->count() == 0)
                            <div class="ui message">
                                Occorre che il cliente abbia almeno un contatto prima di aprire nuove assegnazioni.
                            </div>
                        @else
                            <button class="ui orange basic button async-modal" data-modal-url="{{ route('grant.create', ['customer_id' => $customer->id]) }}">Aggiungi Assegnazione</button>
                        @endif
                    </td>
                </tr>
            </tfoot>
        </table>
    @endif

    <button class="ui positive button" type="submit">Salva</button>
    @if($customer)
        <a class="ui negative button right floated async-modal" href="#" data-modal-url="{{ route('customer.askdestroy', $customer->id) }}">Elimina</a>
    @endif
</form>
