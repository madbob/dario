<div class="ui mini modal">
    <i class="close icon"></i>
    <div class="header">
        Elimina Slot
    </div>
    <div class="content">
        <form method="POST" action="{{ route('slot.destroy', $slot->id) }}" class="ui form">
            @csrf
            @method('DELETE')

            <p>
                Sei sicuro di voler eliminare l'appuntamento {{ $slot->booking->grant->description }} di {{ fullDate($slot->date) }}?
            </p>

            @if($slot->booking->macro_type == 'recurring')
                <p>
                    Questo cancellerà solo la prenotazione per questo giorno, gli altri giorni previsti nella prenotazione rimarranno invariati.
                </p>
            @endif

            <button class="ui negative button" type="submit">Elimina</button>
        </form>
    </div>
</div>
