@extends('layouts.app')

@section('content')
    @include('layouts.menu', ['active' => 'customers'])

    <div class="ui fluid container">
        <div class="ui grid fullwidth">
            <div class="four wide column">
                <div class="ui segment">
                    <div class="ui fluid buttons">
                        <a class="ui basic orange button async-portion" data-portion-url="{{ route('customer.create') }}" data-container-id="customer-details">Aggiungi Cliente</a>

                        @if($display == 'recent')
                            <a class="ui basic orange button" href="{{ route('customer.index', ['display' => 'all']) }}">Vedi Tutti</a>
                        @else
                            <a class="ui basic orange button" href="{{ route('customer.index', ['display' => 'recent']) }}">Vedi Recenti</a>
                        @endif
                    </div>

                    <p></p>

                    <div class="ui search">
                        <div class="ui fluid icon input">
                            <input class="prompt customer-filter" type="text" placeholder="Filtra">
                            <i class="search icon"></i>
                        </div>
                    </div>

                    <div class="ui celled list customer-list">
                        @foreach($customers as $customer)
                            <div class="item">
                                <a class="header async-portion" data-portion-url="{{ route('customer.edit', $customer->id) }}" data-container-id="customer-details">{{ $customer->name }}</a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="twelve wide column" id="customer-details">
                <h2 class="ui center aligned icon header">
                    <i class="circular users icon"></i>
                    Seleziona o crea un cliente.
                </h2>
            </div>
        </div>
    </div>
@endsection
