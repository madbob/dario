<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Room;
use App\RoomType;
use App\Equipment;

class RoomController extends Controller
{
    public function index(Request $request)
    {
        if ($request->user()->role != 'admin')
            abort(403);

        $rooms = Room::orderBy('name', 'asc')->get();
        return view('room.index', compact('rooms'));
    }

    public function update(Request $request, $id)
    {
        if ($request->user()->role != 'admin')
            abort(403);

        $ids = $request->input('id');
        $names = $request->input('name');
        $descriptions = $request->input('description');
        $special_notes = $request->input('special_notes');
        $capacities = $request->input('capacity');
        $prices = $request->input('price');

        $types = [];
        foreach(RoomType::all() as $type) {
            $types[$type->id] = $request->input('type_' . $type->id);
        }

        $equipments = [];
        foreach(Equipment::all() as $equipment) {
            $equipments[$equipment->id] = $request->input('equipment_' . $equipment->id);
            $equipments_prices[$equipment->id] = $request->input('equipment_price_' . $equipment->id);
        }

        $saved = [];

        foreach($ids as $index => $id) {
            $add_to_admin = false;

            if ($id == 'empty') {
                continue;
            }
            else if ($id == 'new') {
                if (empty($names[$index])) {
                    continue;
                }

                $room = new Room();
                $add_to_admin = true;
            }
            else {
                $room = Room::find($id);

                if (empty($names[$index])) {
                    $room->delete();
                    continue;
                }
            }

            $room->name = $names[$index];
            $room->description = $descriptions[$index];
            $room->special_notes = $special_notes[$index];
            $room->capacity = $capacities[$index] ?: 0;
            $room->price = $prices[$index] ?: 0;
            $room->save();

            $room_types = [];
            foreach($types as $type_id => $assignations) {
                if ($assignations[$index] == 1)
                    $room_types[] = $type_id;
            }
            $room->types()->sync($room_types);

            $room_equipments = [];
            foreach($equipments as $equipment_id => $assignations) {
                if ($assignations[$index] == 1) {
                    $room_equipments[$equipment_id] = ['price' => $equipments_prices[$equipment_id][$index]];
                }
            }
            $room->equipments()->sync($room_equipments);

            if ($add_to_admin)
                foreach(User::where('role', 'admin')->get() as $user)
                    $user->rooms()->attach($room->id);

            $saved[] = $room->id;
        }

        Room::whereNotIn('id', $saved)->delete();

        return redirect()->route('room.index');
    }

    public function equipment(Request $request, $id)
    {
        return view('room.equipmentselect', ['room' => $id]);
    }
}
