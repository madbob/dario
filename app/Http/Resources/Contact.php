<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\JsonResource;

class Contact extends JsonResource
{
    public function toArray($request)
    {
        Resource::withoutWrapping();

        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'email' => $this->email,
            'email2' => $this->email2,
            'phone' => $this->phone,
            'mobile' => $this->mobile,
            'edit_url' => route('contact.edit', $this->id)
        ];
    }
}
