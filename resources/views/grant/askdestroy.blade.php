<div class="ui mini modal">
    <i class="close icon"></i>
    <div class="header">
        Elimina Assegnazione
    </div>
    <div class="content">
        <form method="POST" action="{{ route('grant.destroy', $grant->id) }}" class="ui form">
            @csrf
            @method('DELETE')

            <p>
                Sei sicuro di voler eliminare l'assegnazione {{ $grant->description }}?
            </p>

            <button class="ui negative button" type="submit">Elimina</button>
        </form>
    </div>
</div>
