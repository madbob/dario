<tr class="{{ $room ? '' : 'reference-row' }}">
    <td>
        <input type="hidden" name="id[]" value="{{ $room ? $room->id : 'empty' }}">
        <div class="required field">
            <input type="text" name="name[]" value="{{ $room ? $room->name : '' }}">
        </div>
    </td>
    <td>
        <textarea rows="{{ App\RoomType::count() + 1 }}" name="description[]">{{ $room ? $room->description : '' }}</textarea>
    </td>
    <td>
        <textarea rows="{{ App\RoomType::count() + 1 }}" name="special_notes[]">{{ $room ? $room->special_notes : '' }}</textarea>
    </td>
    <td>
        <div class="grouped fields">
            @foreach(App\RoomType::orderBy('name', 'asc')->get() as $type)
                <?php $checked = ($room && $room->hasType($type->id)) ?>
                <div class="field">
                    <div class="ui {{ $checked ? 'checked' : '' }} checkbox explicit">
                        <input type="checkbox" name="{{ \Illuminate\Support\Str::random(10) }}" tabindex="0" class="hidden" {{ $checked ? 'checked' : '' }}>
                        <input type="hidden" name="type_{{ $type->id }}[]" value="{{ $checked ? '1' : '0' }}">
                        <label>{{ $type->name }}</label>
                    </div>
                </div>
            @endforeach
        </div>
    </td>
    <td>
        <div class="grouped fields">
            @foreach(App\Equipment::orderBy('name', 'asc')->get() as $equipment)
                <?php $e = $room ? $room->equipments()->where('equipment.id', $equipment->id)->withPivot('price')->first() : null ?>
                <div class="field">
                    <div class="ui {{ $e ? 'checked' : '' }} checkbox explicit displaying">
                        <input type="checkbox" name="{{ \Illuminate\Support\Str::random(10) }}" tabindex="0" class="hidden" {{ $e ? 'checked' : '' }}>
                        <input type="hidden" name="equipment_{{ $equipment->id }}[]" value="{{ $e ? '1' : '0' }}">
                        <label>{{ $equipment->name }}</label>
                    </div>
                    <div class="ui right labeled fluid input {{ $e ? '' : 'hidden' }} displayable">
                        <input type="number" name="equipment_price_{{ $equipment->id }}[]" value="{{ $e ? $e->pivot->price : 0 }}">
                        <div class="ui basic label">€ / ora</div>
                    </div>
                </div>
            @endforeach
        </div>
    </td>
    <td>
        <div class="ui right labeled fluid input">
            <input type="number" name="capacity[]" value="{{ $room ? $room->capacity : '' }}">
            <div class="ui basic label">posti</div>
        </div>
    </td>
    <td>
        <div class="ui right labeled fluid input">
            <input type="number" name="price[]" value="{{ $room ? $room->price : '' }}">
            <div class="ui basic label">€ / ora</div>
        </div>
    </td>
    <td>
        <button class="ui red icon button pull-right remove-row"><i class="trash icon"></i></button>
    </td>
</tr>
