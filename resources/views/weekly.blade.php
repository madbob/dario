@extends('layouts.app')

@section('content')
    @include('layouts.menu', ['active' => 'home', 'mode' => 'weekly'])

    @if($rooms->count() == 0)
        <h2 class="ui center aligned icon header">
            <i class="circular settings icon"></i>
            Per iniziare, crea almeno una sala.
        </h2>
    @else
		@foreach($days as $index => $day)
            <h2 class="ui center aligned header">{{ $day->format('d/m') }}</h2>

            <table class="ui selectable structured table fixed celled main-grid compact hours_on_top">
                <thead>
                    <tr>
                        <td>&nbsp;</td>
                        @for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
                            <td data-hour="{{ $i }}">{{ $t = floatToTime($i) }}</td>
                        @endfor
                    </tr>
                </thead>
                <tbody>
                    @foreach($rooms as $room)
                        <tr>
                            <td data-room-id="{{ $room->id }}">{{ $room->name }}</td>

                            @for($i = myConf('start_hour'); $i < myConf('end_hour'); $i += 0.5)
                                @if($slot = App\Slot::where('room_id', $room->id)->where('date', $day)->where('from', $t)->where('abolished', false)->first())
                                    @php
                                        $i += $slot->duration_cells;
                                    @endphp

                                    <td class="occupied" data-slot-url="{{ route('slot.edit', $slot->id) }}" rowspan="{{ $slot->duration_cells }}">
                                        <div class="{{ $slot->grid_class }}">
                                            <span class="grant-name">{{ $slot->booking->printable_description }}</span>
                                            <span class="customer-name">{{ $slot->booking->grant->customer->name }}</span>
                                        </div>
                                    </td>
                                @else
                                    <td data-day="{{ $day->format('Y-m-d') }}" data-hour="{{ $i }}">&nbsp;</td>
                                @endif
                            @endfor
                        </tr>
                    @endforeach
                </tbody>
            </table>

            @if($index != 0 && ($index + 1) % 4 == 0)
                <div class="page-break print-only"></div>
            @endif
		@endforeach
    @endif
@endsection
