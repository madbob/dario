@if(empty($days))
    <div class="ui red message">
        <p>
            Non è stato selezionato nessun giorno nell'intervallo definito!
        </p>
    </div>
@else
    <?php

    $excluded_days = [];
    $booking = App\Booking::find($booking_id);
    if ($booking) {
        $excluded_days = $booking->excluded_days;
    }

    ?>

    <table class="ui celled table">
        <thead>
            <tr>
                <th>Giorno</th>
                <th>Ora Inizio</th>
                <th>Ora Fine</th>
                <th>Festa</th>
                <th>Altro</th>
                <th>Azione</th>
            </tr>
        </thead>
        <tbody>
            @foreach($days as $date => $d)
                <?php

                $time_start = floatToTime($d->from);
                $time_end = floatToTime($d->to);

                $existing_booking = App\Slot::getStaticOverlapping($booking_id, $room_id, $date, $time_start, $time_end);
                $existing_slot = $booking ? $booking->slots()->where('room_id', $room_id)->where('date', $date)->where('from', $time_start)->where('to', $time_end)->first() : null;

                ?>

                <tr class="{{ $d->holiday ? 'warning' : ($existing_booking || ($existing_slot && $existing_slot->abolished) ? 'error' : 'positive') }}">
                    <td>{{ fullDate($date) }}</td>
                    <td>{{ $time_start }}</td>
                    <td>{{ $time_end }}</td>
                    <td>{{ $d->holiday ? $d->holiday->name : '' }}</td>
                    <td>{{ $existing_booking ? sprintf('%s (%s/%s)', $existing_booking->booking->description, floatToTime($existing_booking->from), floatToTime($existing_booking->to)) : '' }}</td>

                    <td>
                        @if($existing_booking)
                            <div class="ui radio checkbox">
                                <input type="radio" name="keepers_{{ $existing_booking->id }}" class="hidden ignore-dates-check" value="keep" required {{ in_array($date, $excluded_days) ? '' : 'checked' }}>
                                <label>Mantieni Altra Prenotazione</label>
                            </div>
                            <div class="ui radio checkbox">
                                <input type="radio" name="keepers_{{ $existing_booking->id }}" class="hidden ignore-dates-check" value="replace" required>
                                <label>Usa Questa Prenotazione</label>
                            </div>
                            <div class="ui radio checkbox">
                                <input type="radio" name="keepers_{{ $existing_booking->id }}" class="hidden ignore-dates-check" value="exclude" required>
                                <label>Escludi Giorno da Questa Prenotazione</label>
                            </div>
                        @else
                            @if(!$d->holiday)
                                @if($existing_slot && $existing_slot->abolished)
                                    <label>Rimosso manualmente</label>
                                    <div class="ui checkbox">
                                        <input type="checkbox" name="to_restore[]" class="hidden ignore-dates-check" value="{{ $existing_slot->id }}">
                                        <label>Ripristina</label>
                                    </div>
                                @else
                                    <div class="ui checkbox">
                                        <input type="checkbox" name="exclude_date[]" class="hidden ignore-dates-check" value="{{ $date }}" {{ in_array($date, $excluded_days) ? 'checked' : '' }}>
                                        <label>Escludi Giorno</label>
                                    </div>
                                @endif
                            @endif
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif
