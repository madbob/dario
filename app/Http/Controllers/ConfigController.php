<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Hash;

use App\Config;
use App\User;
use App\Room;
use App\Equipment;
use App\Slot;
use App\Booking;
use App\Payment;
use App\Holiday;

class ConfigController extends Controller
{
    public function index(Request $request)
    {
        if ($request->user()->role != 'admin')
            abort(403);

        return view('config.index');
    }

    public function generic(Request $request)
    {
        if ($request->user()->role != 'admin')
            abort(403);

        $config = Config::where('name', 'start_hour')->first();
        $config->value = $request->input('start_hour');
        $config->save();

        $config = Config::where('name', 'end_hour')->first();
        $config->value = $request->input('end_hour');
        $config->save();

        return redirect()->route('config.index');
    }

    public function users(Request $request)
    {
        if ($request->user()->role != 'admin')
            abort(403);

        $ids = $request->input('id');
        $names = $request->input('name');
        $emails = $request->input('email');
        $passwords = $request->input('password');
        $admins = $request->input('admin');

        $rooms = [];
        foreach(Room::all() as $room) {
            $rooms[$room->id] = $request->input('room_' . $room->id);
        }

        $saved = [];

        foreach($ids as $index => $id) {
            if ($id == 'empty') {
                continue;
            }
            else if ($id == 'new') {
                $user = new User();
            }
            else {
                $user = User::find($id);
            }

            if (empty($emails[$index]))
                continue;

            $user->name = $names[$index];
            $user->email = $emails[$index];

            if (!empty($passwords[$index]))
                $user->password = Hash::make($passwords[$index]);
            else if ($id == 'new')
                $user->password = Hash::make(Str::random(10));

            if ($admins[$index] == 1)
                $user->role = 'admin';
            else
                $user->role = 'manager';

            $user->save();

            $assigned_rooms = [];
            foreach($rooms as $room_id => $assignations) {
                if ($assignations[$index] == 1)
                    $assigned_rooms[] = $room_id;
            }
            $user->rooms()->sync($assigned_rooms);

            $saved[] = $user->id;
        }

        User::whereNotIn('id', $saved)->delete();

        return redirect()->route('config.index');
    }

    public function equipments(Request $request)
    {
        if ($request->user()->role != 'admin')
            abort(403);

        $ids = $request->input('id');
        $names = $request->input('name');

        $saved = [];

        foreach($ids as $index => $id) {
            if ($id == 'empty') {
                continue;
            }
            else if ($id == 'new') {
                $equipment = new Equipment();
            }
            else {
                $equipment = Equipment::find($id);
            }

            $name = $names[$index];
            if (empty(trim($name))) {
                continue;
            }

            $equipment->name = $names[$index];
            $equipment->save();

            $saved[] = $equipment->id;
        }

        Equipment::whereNotIn('id', $saved)->delete();

        return redirect()->route('config.index');
    }

    public function review()
    {
        $holiday_dates = Holiday::select('date')->get()->pluck('date');
        $booking_ids = Slot::select('booking_id')->distinct()->whereIn('date', $holiday_dates)->pluck('booking_id');
        Slot::select('booking_id')->distinct()->whereIn('date', $holiday_dates)->delete();
        $bookings = Booking::whereIn('id', $booking_ids)->where('payment', 'pay')->get();

        $qs = ['first', 'second', 'third', 'fourth'];

        foreach($bookings as $booking) {
            $days = $booking->getDays(false);
            $quarters = Booking::sortDaysByQuarter($days);

            foreach($qs as $q)
                Payment::where('booking_id', $booking->id)->where('payed', false)->where('quarter', $q)->update(['amount' => $quarters->$q->days * $booking->unit_price]);
        }

        return redirect()->route('config.index');
    }
}
