<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Events\SluggableCreating;

class Customer extends Model
{
    use SluggableID;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $dispatchesEvents = [
        'creating' => SluggableCreating::class,
    ];

    public function grants()
    {
        return $this->hasMany('App\Grant')->orderBy('year', 'desc');
    }

    public function contacts()
    {
        return $this->belongsToMany('App\Contact')->orderBy('name', 'asc');
    }

    public function scopeRecent($query)
    {
        $query->whereHas('grants', function($query) {
            $query->whereHas('bookings', function($query) {
                $query->where('end', '>=', date('Y-m-d', strtotime('-1 years')));
            });
        });
    }

    public function readRequest($request, $prefix = '')
    {
        $this->name = $request->input($prefix . 'name');
        $this->type = $request->input($prefix . 'type');
        $this->address = $request->input($prefix . 'address');
        $this->city = $request->input($prefix . 'city');
        $this->zip = $request->input($prefix . 'zip');
        $this->vat = $request->input($prefix . 'vat');
        $this->taxcode = $request->input($prefix . 'taxcode');
        $this->phone = $request->input($prefix . 'phone');
        $this->email = $request->input($prefix . 'email');
        $this->website = $request->input($prefix . 'website');
        $this->discount = $request->input($prefix . 'discount', 0);
        $this->notes = $request->input($prefix . 'notes');
    }

    public function readContacts($request, $field = 'contact_id')
    {
        $ids = array_filter($request->input($field));

        $ids = array_filter($ids, function($item) {
            return $item != 'new';
        });

        $this->contacts()->sync($ids);
    }
}
