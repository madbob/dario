<div class="ui large modal">
    <i class="close icon"></i>
    <div class="header">
        @if($year)
            Modifica Anno Sociale
        @else
            Nuovo Anno Sociale
        @endif
    </div>
    <div class="content">
        <form method="POST" action="{{ $year ? route('year.update', $year->id) : route('year.store') }}" class="ui form">
            @csrf

            @if($year)
                @method('PUT')
            @endif

            <div class="field">
                <label>Anno</label>
                <input type="hidden" name="year" value="{{ $year ? $year->name : $string_year }}">
                <p>{{ $year ? $year->name : $string_year }}</p>
            </div>

            <div class="two fields">
                <div class="required field ui calendar">
                    <label>Inizio Anno Sociale</label>
                    <input type="text" name="start" value="{{ $year ? widgetDate($year->start) : '01/09/' . substr($string_year, 0, strpos($string_year, '/')) }}" required>
                </div>

                <div class="required field ui calendar">
                    <label>Fine Anno Sociale</label>
                    <input type="text" name="end" value="{{ $year ? widgetDate($year->end) : '31/07/' . substr($string_year, strpos($string_year, '/') + 1) }}" required>
                </div>
            </div>

            <h4 class="ui horizontal divider header">
                <i class="thumbtack icon"></i>
                Festività
            </h4>

            <table id="holidays" class="ui basic table dynamic-table">
                <thead>
                    <tr>
                        <th width="45%">Nome</th>
                        <th width="45%">Data</th>
                        <th width="10%">&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($year ? $year->holidays : $prev_year->holidays as $holiday)
                        <?php if (!$year) $holiday->date = $holiday->nextDate() ?>
                        @include('holiday.edit', ['holiday' => $holiday])
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="2">
                            <button class="ui orange basic button add-remote-row" data-remote-url="{{ route('holiday.create') }}">Aggiungi Festività</button>
                        </td>
                    </tr>
                </tfoot>
            </table>

            @if($year == null)
                <h4 class="ui horizontal divider header">
                    <i class="history icon"></i>
                    Rinnovo Assegnazioni
                </h4>

                <table id="holidays" class="ui basic table dynamic-table">
                    <thead>
                        <tr>
                            <th width="20%">Cliente</th>
                            <th width="20%">Descrizione</th>
                            <th width="55%">Occupazione</th>
                            <th width="5%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($prev_year->grants as $grant)
                            <tr>
                                <td>{{ $grant->customer->name }}</td>
                                <td>{{ $grant->description }}</td>
                                <td>{!! nl2br($grant->year_human_summary) !!}</td>
                                <td>
                                    <div class="ui checked toggle checkbox explicit">
                                        <input type="checkbox" name="grants[]" value="{{ $grant->id }}" tabindex="0" class="hidden" checked>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif

            <button class="ui positive button" type="submit">Salva</button>
        </form>
    </div>
</div>
