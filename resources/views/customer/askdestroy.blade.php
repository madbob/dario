<div class="ui mini modal">
    <i class="close icon"></i>
    <div class="header">
        Elimina Cliente
    </div>
    <div class="content">
        <form method="POST" action="{{ route('customer.destroy', $customer->id) }}" class="ui form">
            @csrf
            @method('DELETE')

            <p>
                Sei sicuro di voler eliminare il cliente {{ $customer->name }}?
            </p>

            <button class="ui negative button" type="submit">Elimina</button>
        </form>
    </div>
</div>
