<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\JsonResource;

class Grant extends JsonResource
{
    public function toArray($request)
    {
        Resource::withoutWrapping();

        return [
            'id' => $this->id,
            'customer_name' => $this->customer->name,
            'year' => $this->year,
            'number' => $this->number,
            'types' => $this->human_type,
            'description' => $this->description,
            'bookings' => Booking::collection($this->bookings),
            'payments' => Payment::collection($this->payments),
            'edit_url' => route('grant.edit', $this->id),
            'destroy_url' => route('grant.askdestroy', $this->id)
        ];
    }
}
