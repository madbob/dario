<div>
    <div class="required field">
        <label>Sala</label>
        <select class="select-loader" data-load-area=".equipment-select" name="room_id" required>
            @foreach($currentuser->rooms()->orderBy('name', 'asc')->get() as $room)
                <option value="{{ $room->id }}" {{ $selected_room == $room->id ? 'selected' : '' }} data-load-url="{{ route('room.equipment', $room->id) }}">{{ $room->name_with_types }}</option>
            @endforeach
        </select>
    </div>

    @include('room.equipmentselect', ['room' => $selected_room, 'selected' => $selected_equipment])

    <table id="manual_days_selector" class="ui very basic table dynamic-table">
        <tbody>
            @foreach($details->days as $d)
                @include('booking.spaces.manual_day', ['visible' => true, 'day' => $d->date, 'from' => $d->from, 'to' => $d->to])
            @endforeach

            @include('booking.spaces.manual_day', ['visible' => false, 'day' => null, 'from' => $details->days[0]->from, 'to' => $details->days[0]->to])
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <button class="ui orange basic button add-row" data-table-target="#rooms">Aggiungi Giorno</button>
                    <a class="ui basic button test-booking-days async-portion" href="#" data-portion-url="{{ route('booking.wizard', ['page' => 'test']) }}" data-container-id="days_tester">Verifica Giorni</a>
                </td>
            </tr>
        </tfoot>
    </table>

    <div id="days_tester"></div>
</div>
