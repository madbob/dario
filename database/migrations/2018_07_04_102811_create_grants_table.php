<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGrantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('grants', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->enum('type', ['party', 'course', 'internal', 'other']);
            $table->string('description');
            $table->string('customer_id');
            $table->string('contact_id')->default('');
            $table->string('year');
            $table->date('signature')->nullable();
            $table->integer('number')->unsigned()->nullable();
            $table->date('protocol')->nullable();
            $table->text('notes');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grants');
    }
}
