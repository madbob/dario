<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Log;

class Config extends Model
{
    public static function provideDefault($name)
    {
        $defaults = [
            'start_hour' => 8,
            'end_hour' => 24
        ];

        if (isset($defaults[$name])) {
            $c = new Config();
            $c->name = $name;
            $c->value = $defaults[$name];
            $c->save();
            return $c;
        }
        else {
            Log::error('Configuration type not found: ' . $name);
            return null;
        }
    }
}
