<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Log;

use App\Events\SluggableCreating;

class Year extends Model
{
    use SluggableID;

    protected $keyType = 'string';
    public $incrementing = false;

    protected $dispatchesEvents = [
        'creating' => SluggableCreating::class,
    ];

    public function holidays()
    {
        return $this->hasMany('App\Holiday', 'year')->orderBy('date', 'asc');
    }

    public function grants()
    {
        return $this->hasMany('App\Grant', 'year');
    }

    public function getYearlyGrantsAttribute()
    {
        return $this->grants()->whereHas('bookings', function($query) {
            $query->yearly();
        })->get();
    }
}
