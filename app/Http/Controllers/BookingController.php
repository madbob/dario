<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use DB;

use App\Customer;
use App\Grant;
use App\Year;
use App\Room;
use App\Booking;
use App\Payment;
use App\Holiday;

class BookingController extends Controller
{
    public function create(Request $request)
    {
        $grant = Grant::find($request->input('grant_id', -1));

        $selected_day = $request->input('day');
        $selected_room = $request->input('room');
        $selected_equipment = [];

        $selected_start_hour = $request->input('hour');
        $selected_end_hour = $selected_start_hour + 1;
        $selected_start_hour = floatToTime($selected_start_hour);
        if ($selected_end_hour > myConf('end_hour'))
            $selected_end_hour = $selected_start_hour + 0.5;
        $selected_end_hour = floatToTime($selected_end_hour);

        return view('booking.create', compact('grant', 'selected_room', 'selected_equipment', 'selected_day', 'selected_start_hour', 'selected_end_hour'));
    }

    private function buildPeriodInfo($request)
    {
        $type = $request->input('type');

        switch($type) {
            case 'recurring':
                $main_type = 'recurring';

                $days_in_week = $request->input('day');
                $from_hours = $request->input('from');
                $to_hours = $request->input('to');

                $cycle = $request->input('week_cycle');
                if ($cycle == 'none')
                    $cycle = $request->input('monthly_cycle');
                if (!is_array($cycle))
                    $cycle = [$cycle];

                $end_criteria = $request->input('end_criteria');
                $end = (object) [
                    'criteria' => $end_criteria
                ];

                switch($end_criteria) {
                    case 'all_year':
                        $end->yearly = 1;
                        break;
                    case 'given_day':
                        $end->date = $request->input('end_day');
                        break;
                    case 'given_number':
                        $end->number = $request->input('given_number_value');
                        break;
                }

                $days = [];

                foreach($days_in_week as $diw_index => $diw) {
                    if ($diw == -1)
                        continue;

                    $days[] = (object) [
                        'offset' => $diw,
                        'from' => $from_hours[$diw_index],
                        'to' => $to_hours[$diw_index],
                    ];
                }

                break;

            case 'occasional':
                $main_type = 'single';

                $cycle = ['all'];
                $start_date = readDate($request->input('start_day'));
                $end_date = readDate($request->input('end_day'));

                $end = (object) [
                    'criteria' => 'date',
                    'date' => $end_date
                ];

                $days = [];
                $all_days = new \DatePeriod(new \DateTime($start_date), new \DateInterval('P1D'), (new \DateTime($end_date))->modify('+1 days'));
                foreach($all_days as $d) {
                    $days[] = (object) [
                        'offset' => $d->format('w'),
                        'from' => $request->input('from'),
                        'to' => $request->input('to'),
                    ];
                }

                break;

            case 'manual':
                $main_type = 'manual';

                $cycle = ['manual'];

                $end = (object) [
                    'criteria' => 'manual'
                ];

                $ids = $request->input('choosen_day_id', []);
                $choosen_days = $request->input('choosen_days', []);
                $choosen_start = $request->input('from', []);
                $choosen_end = $request->input('to', []);
                $days = [];

                foreach($ids as $index => $id) {
                    if ($id != 'new') {
                        continue;
                    }

                    $days[] = (object) [
                        'date' => readDate($choosen_days[$index]),
                        'from' => $choosen_start[$index],
                        'to' => $choosen_end[$index]
                    ];
                }

                break;
        }

        $info = (object) [
            'type' => $main_type,
            'cycle' => $cycle,
            'end' => $end,
            'excluded' => $request->input('exclude_date', []),
            'days' => $days
        ];

        return $info;
    }

    private function buildDefaultPeriodInfo($request)
    {
        return (object) [
            'type' => 'recurring',
            'cycle' => ['all'],
            'end' => (object) [
                'criteria' => 'all_year',
            ],
            'excluded' => [],
            'days' => [
                (object) [
                    'offset' => -1,
                    'from' => $request->input('selected_start_hour'),
                    'to' => $request->input('selected_end_hour'),
                ]
            ]
        ];
    }

    private function buildDefaultManualPeriodInfo($request)
    {
        return (object) [
            'type' => 'manual',
            'cycle' => ['manual'],
            'end' => (object) [
                'criteria' => 'manual',
            ],
            'excluded' => [],
            'days' => [
                (object) [
                    'date' => $request->input('selected_day'),
                    'from' => $request->input('selected_start_hour'),
                    'to' => $request->input('selected_end_hour'),
                ]
            ]
        ];
    }

    private function getDaysRaw($request, $include_holidays = true)
    {
        $start = new \DateTime(readDate($request->input('start_day')));
        $info = self::buildPeriodInfo($request);
        $type = $request->input('type');

        switch($type) {
            case 'recurring':
                $end_criteria = $request->input('end_criteria');

                switch($end_criteria) {
                    case 'all_year':
                        $year = currentSocialYear();
                        $end = new \DateTime($year->end);
                        $days = Booking::staticGetDays($start, $end, $info, $include_holidays);
                        break;
                    case 'given_day':
                        $end = new \DateTime(readDate($request->input('end_day')));
                        $days = Booking::staticGetDays($start, $end, $info, $include_holidays);
                        break;
                    case 'given_number':
                        $days_number = $request->input('given_number_value');
                        if (is_numeric($days_number)) {
                            $end = clone $start;
                            $end->modify('+1 years');
                            $days = Booking::staticGetDays($start, $end, $info, $include_holidays);

                            $holidays_num = array_reduce($days, function($carry, $item) {
                                if (!is_null($item->holiday))
                                    return $carry + 1;
                                else
                                    return $carry;
                            }, 0);

                            $days = array_splice($days, 0, $days_number + $holidays_num);
                        }
                        else {
                            $days = [];
                        }

                        break;
                }

                break;

            case 'occasional':
                $end = new \DateTime(readDate($request->input('end_day')));
                $days = Booking::staticGetDays($start, $end, $info, $include_holidays);
                break;

            case 'manual':
                /*
                    Nella tabella dinamica coi giorni, per isolare le righe
                    buone da quella di riferimento viene aggiunto un ID
                    farlocco. Quelle buone hanno ID = 'new', in quella di
                    riferimento ne viene settato uno farlocco per identificarla
                    e saltarla
                */
                $ids = $request->input('choosen_day_id', []);
                $choosen_days = $request->input('choosen_days', []);
                $choosen_start = $request->input('from', []);
                $choosen_end = $request->input('to', []);
                $days = [];
                $starts = [];
                $ends = [];

                foreach($ids as $index => $id) {
                    if ($id != 'new') {
                        continue;
                    }

                    $days[] = readDate($choosen_days[$index]);
                    $starts[] = $choosen_start[$index];
                    $ends[] = $choosen_end[$index];
                }

                $days = Booking::staticGetManualDays($days, $starts, $ends, $include_holidays);
                break;
        }

        return $days;
    }

    public function wizard(Request $request, $page)
    {
        switch ($page) {
            case 'search_customer':
                return view('booking.customers.search');

            case 'new_customer':
                return view('booking.customers.new');

            case 'customers':
                $customer_id = $request->input('customer_id');
                if (empty($customer_id) || $customer_id == 'new')
                    $customer = new Customer();
                else
                    $customer = Customer::find($customer_id);

                return view('booking.customers.grants', compact('customer'));
                break;

            case 'spaces':
                $type = $request->input('type');

                switch($type) {
                    case 'recurring':
                        return view('booking.spaces.recurring', [
                            'selected_room' => $request->input('selected_room'),
                            'selected_equipment' => [],
                            'selected_day' => $request->input('selected_day'),
                            'details' => $this->buildDefaultPeriodInfo($request)
                        ]);
                        break;
                    case 'occasional':
                        return view('booking.spaces.single', [
                            'selected_room' => $request->input('selected_room'),
                            'selected_equipment' => [],
                            'selected_day' => $request->input('selected_day'),
                            'selected_start_hour' => $request->input('selected_start_hour'),
                            'selected_end_hour' => $request->input('selected_end_hour'),
                        ]);
                        break;
                    case 'manual':
                        return view('booking.spaces.manual', [
                            'selected_room' => $request->input('selected_room'),
                            'selected_equipment' => [],
                            'details' => $this->buildDefaultManualPeriodInfo($request)
                        ]);
                        break;
                }

                break;

            case 'test':
                $room_id = $request->input('room_id');
                $days = self::getDaysRaw($request, true);
                $booking_id = $request->input('booking_id');
                return view('booking.test.list', compact('booking_id', 'days', 'room_id'));
                break;

            case 'payments':
                $room_id = $request->input('room_id');
                if (empty($room_id))
                    $room_id = $request->input('selected_room');
                $selected_room = Room::find($room_id);

                $payment = $request->input('payment');
                switch($payment) {
                    case 'pay':
                        if ($request->has('booking_id')) {
                            $booking_id = $request->input('booking_id');
                            $booking = Booking::find($booking_id);
                            $unit_price = $booking->unit_price;
                            $total_price = $booking->total_price;
                            $days = $booking->getDays(false);
                            $payments = $booking->payments;
                        }
                        else {
                            $unit_price = $request->input('unit_price');
                            $total_price = $request->input('total_price');
                            $days = self::getDaysRaw($request, false);
                            $payments = null;
                        }

                        $quarters = Booking::sortDaysByQuarter($days);

                        if ($payments == null || $payments->isEmpty()) {
                            $payments = [];

                            /*
                                L'attributo "involved_days" viene valorizzato ad
                                uso e consumo della vista, per permettere a
                                posteriori di effettuare i calcoli in JS
                            */

                            $pay = new Payment();
                            $pay->description = 'Primo Trimestre';
                            $pay->quarter = 'first';
                            $pay->amount = $quarters->first->days * $unit_price;
                            $pay->date = $quarters->first->year . '-03-31';
                            $pay->payed = false;
                            $pay->involved_days = $quarters->first->days;
                            $payments[] = $pay;

                            $pay = new Payment();
                            $pay->description = 'Secondo Trimestre';
                            $pay->quarter = 'second';
                            $pay->amount = $quarters->second->days * $unit_price;
                            $pay->date = $quarters->second->year . '-06-30';
                            $pay->payed = false;
                            $pay->involved_days = $quarters->second->days;
                            $payments[] = $pay;

                            $pay = new Payment();
                            $pay->description = 'Terzo Trimestre';
                            $pay->quarter = 'third';
                            $pay->amount = $quarters->third->days * $unit_price;
                            $pay->date = $quarters->third->year . '-09-30';
                            $pay->payed = false;
                            $pay->involved_days = $quarters->third->days;
                            $payments[] = $pay;

                            $pay = new Payment();
                            $pay->description = 'Quarto Trimestre';
                            $pay->quarter = 'fourth';
                            $pay->amount = $quarters->fourth->days * $unit_price;
                            $pay->date = $quarters->fourth->year . '-12-31';
                            $pay->payed = false;
                            $pay->involved_days = $quarters->fourth->days;
                            $payments[] = $pay;
                        }

                        return view('booking.payments.full', [
                            'days' => $days,
                            'unit_price' => $unit_price,
                            'total_price' => $total_price,
                            'payments' => $payments
                        ]);
                        break;

                    case 'free':
                        return view('booking.payments.free');
                        break;

                    case 'contribute':
                        if ($request->has('booking_id')) {
                            $booking_id = $request->input('booking_id');
                            $booking = Booking::find($booking_id);
                            $unit_price = $booking->unit_price;
                            $total_price = $booking->total_price;
                            $days = $booking->getDays(false);
                            $payment = $booking->payments->first();
                        }
                        else {
                            $unit_price = $request->input('unit_price');
                            $total_price = $request->input('total_price');
                            $days = self::getDaysRaw($request, false);

                            $payment = new Payment();
                            $payment->description = 'Pagamento';
                            $payment->amount = count($days) * $unit_price;
                            $payment->date = key(array_slice($days, -1, 1, true));
                            $payment->payed = false;
                            $payment->involved_days = count($days);
                        }

                        return view('booking.payments.single', [
                            'days' => $days,
                            'unit_price' => $unit_price,
                            'total_price' => $total_price,
                            'payment' => $payment
                        ]);
                        break;
                }
                break;

            case 'single-payment':
                $payment = null;
                return view('booking.payments.payment', compact('payment'));
                break;
        }
    }

    private function rebuildBooking($booking, $request)
    {
        $recursion = self::buildPeriodInfo($request);
        $booking->recursion = json_encode($recursion);

        $days = self::getDaysRaw($request, false);
        $first_computed_day = array_keys($days)[0];
        $last_computed_day = array_keys($days)[count($days) - 1];

        $booking->description = $request->input('description') ?: '';
        $booking->type = $request->input('type');
        $booking->room_id = $request->input('room_id');
        $booking->start = new \DateTime($first_computed_day);
        $booking->end = new \DateTime($last_computed_day);
        $booking->color = $request->input('color', 'red');
        $booking->payment = $request->input('payment', 'free');
        $booking->unit_price = $request->input('unit_price') ?: 0;
        $booking->total_price = $request->input('total_price') ?: 0;
        $booking->partecipants = $request->input('partecipants', 0);
        $booking->save();

        $equipments = $request->input('equipment_id', []);
        $booking->equipments()->sync($equipments);

        return $booking;
    }

    private function rebuildPayments($booking, $request)
    {
        $payment_type = $request->input('payment');

        $booking->payments()->delete();

        if ($payment_type == 'pay') {
            Payment::saveBatch($request, $booking);
        }
        else if ($payment_type == 'free') {
            /* dummy */
        }
        else {
            $pay = new Payment();
            $pay->booking_id = $booking->id;
            $pay->description = 'Contributo';
            $pay->amount = $request->input('total_price');
            $pay->date = readDate($request->input('payment_date'));
            $pay->payed = $request->has('payment_payed');
            $pay->save();
        }

        $booking->deposit = $request->input('deposit');
        if (empty($booking->deposit))
            $booking->deposit = 0;

        $booking->deposit_status = $request->input('deposit_status');
        $booking->save();
    }

    private function allocateGrant($request)
    {
        $customer_id = $request->input('customer_id');
        if (empty($customer_id) || $customer_id == 'new') {
            $customer = new Customer();
            $customer->readRequest($request, 'customer_');
            $customer->save();
        }
        else {
            $customer = Customer::find($customer_id);
        }

        $grant = new Grant();
        $grant->type = $request->input('grant_type') ?: 'other';
        $grant->customer_id = $customer->id;
        $grant->contact_id = $request->input('contact_id') ?: 0;
        $grant->description = $request->input('grant_description') ?: '';

        $grant->year = $request->input('grant_year');
        if (is_null($grant->year)) {
            $grant->year = currentSocialYear()->id;
        }

        $grant->number = $request->input('grant_number') ?: 0;
        $grant->protocol = $request->input('grant_protocol') ? readDate($request->input('grant_protocol')) : null;
        $grant->signature = $request->input('grant_signature') ? readDate($request->input('grant_signature')) : null;
        $grant->notes = $request->input('grant_notes') ?: '';
        $grant->save();

        return $grant;
    }

    public function store(Request $request)
    {
        DB::beginTransaction();

        $grant_id = $request->input('grant_id');
        if (empty($grant_id) || $grant_id == 'new') {
            $grant = $this->allocateGrant($request);
        }
        else {
            $grant = Grant::find($grant_id);
            if (is_null($grant)) {
                $grant = $this->allocateGrant($request);
            }
        }

        $booking = new Booking();
        $booking->grant_id = $grant->id;
        $booking = self::rebuildBooking($booking, $request);

        self::rebuildPayments($booking, $request);
        $booking->generateSlots($request);

        DB::commit();

        return redirect()->back();
    }

    public function edit($id)
    {
        $booking = Booking::find($id);
        return view('booking.edit', compact('booking'));
    }

    public function update(Request $request, $id)
    {
        DB::beginTransaction();

        $booking = Booking::find($id);

        $grant = $booking->grant;
        $grant->description = $request->input('description') ?: '';
        $grant->number = $request->input('grant_number') ?: 0;
        $grant->contact_id = $request->input('contact_id') ?: 0;
        $grant->protocol = $request->input('grant_protocol') ? readDate($request->input('grant_protocol')) : null;
        $grant->signature = $request->input('grant_signature') ? readDate($request->input('grant_signature')) : null;
        $grant->notes = $request->input('grant_notes');
        $grant->save();

        $booking = self::rebuildBooking($booking, $request);

        self::rebuildPayments($booking, $request);
        $booking->generateSlots($request);

        DB::commit();

        return redirect()->back();
    }

    public function askdestroy($id)
    {
        $booking = Booking::find($id);
        return view('booking.askdestroy', compact('booking'));
    }

    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();

        $booking = Booking::find($id);
        $mode = $request->input('mode', 'all');

        if ($mode == 'all') {
            $booking->slots()->delete();
            $booking->delete();
        }
        else if ($mode == 'partial') {
            $today = date('Y-m-d');
            $booking->slots()->where('date', '>', $today)->delete();

            $recursion = $booking->recursion_obj;
            $recursion->end = (object) [
                'criteria' => 'date',
                'date' => $today
            ];
            $booking->recursion = json_encode($recursion);
            $booking->save();
        }

        DB::commit();

        return redirect()->back();
    }
}
