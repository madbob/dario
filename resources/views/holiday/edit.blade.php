<tr>
    <td>
        <input type="text" name="holiday_name[]" value="{{ $holiday ? $holiday->name : '' }}">
    </td>
    <td>
        <div class="field ui calendar">
            <input type="text" name="holiday_date[]" value="{{ $holiday ? widgetDate($holiday->date) : '' }}">
        </div>
    </td>
    <td>
        <span class="pull-right">
            <button class="ui red icon button remove-row"><i class="trash icon"></i></button>
        </span>
    </td>
</tr>
