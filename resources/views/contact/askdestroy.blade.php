<div class="ui mini modal">
    <i class="close icon"></i>
    <div class="header">
        Elimina Contatto
    </div>
    <div class="content">
        <form method="POST" action="{{ route('contact.destroy', $contact->id) }}" class="ui form">
            @csrf
            @method('DELETE')

            <p>
                Sei sicuro di voler eliminare il contatto {{ $contact->full_name }}?
            </p>
            <p>
                Numero di clienti assegnati: {{ $contact->customers()->count() }}
            </p>

            <button class="ui negative button" type="submit">Elimina</button>
        </form>
    </div>
</div>
