<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Http\Resources\Json\JsonResource;

class Booking extends JsonResource
{
    public function toArray($request)
    {
        Resource::withoutWrapping();

        return [
            'description' => $this->description,
        ];
    }
}
