<div class="ui inverted fixed menu no-print">
    <div class="ui container fluid">
        <a class="ui simple item {{ $active == 'home' ? 'active' : '' }}" href="{{ route('home') }}">Calendario</a>
        <a class="ui simple item {{ $active == 'customers' ? 'active' : '' }}" href="{{ route('customer.index') }}">Clienti</a>
        <a class="ui simple item {{ $active == 'contacts' ? 'active' : '' }}" href="{{ route('contact.index') }}">Contatti</a>
        <a class="ui simple item {{ $active == 'grants' ? 'active' : '' }}" href="{{ route('grant.index') }}">Assegnazioni</a>

        @if($currentuser->role == 'admin')
            <a class="ui simple item {{ $active == 'rooms' ? 'active' : '' }}" href="{{ route('room.index') }}">Sale</a>
            <a class="ui simple item {{ $active == 'config' ? 'active' : '' }}" href="{{ route('config.index') }}">Configurazioni</a>
        @endif

        <div class="right menu">
            @if($active == 'home')
                @if($rooms->count() != 0)
                    <div class="ui simple dropdown item" id="room-navigator">
                        @if($room)
                            <input type="hidden" name="current_selected_room" value="{{ $room->id }}">
                            {{ $room->name }}
                        @else
                            @if($mode == 'day')
                                <input type="hidden" name="current_selected_room" value="0">
                            @else
                                <input type="hidden" name="current_selected_room" value="999">
                            @endif

                            Seleziona Sala
                        @endif
                        <i class="dropdown icon"></i>

                        <div class="menu">
                            <a class="item" data-room-id="0">Tutte le Sale (giornaliero)</a>
                            <a class="item" data-room-id="999">Tutte le Sale (settimanale)</a>
                            @foreach($rooms as $room)
                                <a class="item" data-room-id="{{ $room->id }}">{{ $room->name }}</a>
                            @endforeach
                        </div>
                    </div>
                @endif

                <?php $formatted_day = isset($day) ? $day->format('Y-m-d') : $days->getStartDate()->format('Y-m-d') ?>

                <div class="ui simple item">
                    <input type="hidden" name="current_selected_day" value="{{ isset($day) ? $day->format('Y-m-d') : $days->getStartDate()->format('Y-m-d') }}">

                    <div class="ui inverted icon button" id="navigator-prev">
                        <i class="arrow alternate circle left icon"></i>
                    </div>

                    <div class="ui calendar navigation" id="day-navigator">
                        <div class="ui basic inverted button">{{ fullDate(isset($day) ? $day : $days->getStartDate()) }} <i class="dropdown icon"></i></div>
                    </div>

                    <div class="ui inverted icon button" id="navigator-next">
                        <i class="arrow alternate circle right icon"></i>
                    </div>
                </div>

                <div class="ui simple item">
                    <div class="ui inverted icon button print-page">
                        <i class="print icon"></i>
                    </div>
                </div>
            @endif

            <div class="ui simple item">
                {{ $currentuser->name }}&nbsp;
                <div class="ui inverted icon button" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                    <i class="power off icon"></i>
                </div>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </div>
        </div>
    </div>
</div>
