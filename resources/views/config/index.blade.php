@extends('layouts.app')

@section('content')
    @include('layouts.menu', ['active' => 'config'])

    <div class="ui grid container">
        <?php

        $holiday_dates = App\Holiday::select('date')->get()->pluck('date');
        $overlapping_count = App\Slot::whereIn('date', $holiday_dates)->count();

        ?>

        @if($overlapping_count != 0)
            <div class="sixteen wide column">
                <div class="ui negative message">
                    <p>
                        Attenzione! Ci sono prenotazioni che includono giorni di chiusura! Cliccando il tasto sotto gli slot nei giorni di chiusura verranno eliminati e saranno ricalcolati i pagamenti (non ancora pagati) delle relative prenotazioni.
                    </p>
                    <form method="POST" action="{{ route('config.review') }}">
                        @csrf
                        <button type="submit" class="ui red button">Elimina slot e ricalcola pagamenti</button>
                    </form>
                </div>
            </div>
        @endif

        <div class="sixteen wide column">
            <h3 class="ui top attached header">
                Configurazioni Globali
            </h3>
            <div class="ui attached segment">
                <form method="POST" action="{{ route('config.generic') }}" class="ui form">
                    @csrf

                    <div class="two fields">
                        <div class="field">
                            <label>Ora Inizio</label>
                            <input type="number" name="start_hour" value="{{ myConf('start_hour') }}">
                        </div>
                        <div class="field">
                            <label>Ora Fine</label>
                            <input type="number" name="end_hour" value="{{ myConf('end_hour') }}">
                        </div>
                    </div>

                    <button class="ui positive button" type="submit">Salva</button>
                </form>
            </div>

            <h3 class="ui top attached header">
                Anni Sociali
            </h3>
            <div class="ui attached segment">
                <table id="years" class="ui basic table">
                    <thead>
                        <tr>
                            <th width="30%">Anno</th>
                            <th width="30%">Inizio</th>
                            <th width="30%">Fine</th>
                            <th width="10%">&nbsp;</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach(App\Year::orderBy('name', 'desc')->get() as $year)
                            <tr>
                                <td>{{ $year->name }}</td>
                                <td>{{ fullDate($year->start) }}</td>
                                <td>{{ fullDate($year->end) }}</td>
                                <td>
                                    <button class="ui blue icon button async-modal" data-modal-url="{{ route('year.edit', $year->id) }}"><i class="zoom in icon"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td>
                                <button class="ui orange basic button async-modal" data-modal-url="{{ route('year.create') }}">Aggiungi Anno</button>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>

            <h3 class="ui top attached header">
                Utenti
            </h3>
            <div class="ui attached segment">
                <form method="POST" action="{{ route('config.users') }}" class="ui form">
                    @csrf

                    <table id="rooms" class="ui very basic table dynamic-table">
                        <thead>
                            <tr>
                                <th width="20%">Nome</th>
                                <th width="20%">E-Mail</th>
                                <th width="20%">Password<br>(lascia in bianco per non modificare)</th>
                                <th width="30%">Sale Gestite</th>
                                <th width="5%">Amministratore</th>
                                <th width="5%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(App\User::orderBy('name', 'asc')->get() as $user)
                                @include('user.edit', ['user' => $user])
                            @endforeach

                            @include('user.edit', ['user' => null])
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2">
                                    <button class="ui orange basic button add-row">Aggiungi Utente</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                    <button class="ui positive button" type="submit">Salva</button>
                </form>
            </div>

            <h3 class="ui top attached header">
                Attrezzature
            </h3>
            <div class="ui attached segment">
                <form method="POST" action="{{ route('config.equipments') }}" class="ui form">
                    @csrf

                    <table id="equipments" class="ui basic table dynamic-table">
                        <thead>
                            <tr>
                                <th width="95%">Nome</th>
                                <th width="5%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach(App\Equipment::orderBy('name', 'asc')->get() as $equipment)
                                @include('equipment.edit', ['equipment' => $equipment])
                            @endforeach

                            @include('equipment.edit', ['equipment' => null])
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>
                                    <button class="ui orange basic button add-row" data-table-target="#equipments">Aggiungi Attrezzatura</button>
                                </td>
                            </tr>
                        </tfoot>
                    </table>

                    <div class="ui divider"></div>

                    <button class="ui positive button" type="submit">Salva</button>
                </form>
            </div>
        </div>
    </div>
@endsection
