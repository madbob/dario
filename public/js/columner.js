(function ($) {
    $.fn.extend({
        _updateVisibleColumns: function(settings, widget) {
            var visible_count = 0;
            var total_count = 0;
            var table = $(this);
            var selected = new Array();
            var column_indexes = new Array();

            widget.find('input:checkbox').each(function() {
                total_count += 1;
                var identifier = $(this).val();
                var column_index = table.find('thead').find('th[data-columner-target=' + identifier + ']').index() + 1;

                if ($(this).prop('checked')) {
                    table.find('thead th').filter(':nth-child(' + column_index + ')').show();
                    table.find('tbody tr td').filter(':nth-child(' + column_index + ')').show();
                    selected.push(identifier);
                    column_indexes.push(column_index);
                    visible_count += 1;
                }
                else {
                    table.find('thead th').filter(':nth-child(' + column_index + ')').hide();
                    table.find('tbody tr td').filter(':nth-child(' + column_index + ')').hide();
                }
            });

            Cookies.set(settings.cookie_name, selected);

            var first_column_width = $(this).find('thead th:first').outerWidth();
            var column_percentage_width = (100 - ((100 * first_column_width) / $(this).width())) / visible_count;

            for(var i = 0; i < column_indexes.length; i++) {
                table.find('thead th').filter(':nth-child(' + column_indexes[i] + ')').css('width', column_percentage_width + '%');
            }

            widget.toggleClass('red', (total_count != visible_count));
            widget.trigger('columns-updated');
        }
    });

    $.fn.columner = function(parameters) {
        var settings = $.isPlainObject(parameters) ? $.extend(true, {}, $.fn.columner.settings, parameters) : $.extend({}, $.fn.columner.settings);

        $(this).each(function() {
            var widget = $(this);
            var table = $(this).closest('table');
            var selected = Cookies.getJSON(settings.cookie_name);

            if (selected == null) {
                selected = new Array();
                $(this).find('input:checkbox').each(function() {
                    selected.push($(this).val());
                });
                Cookies.set(settings.cookie_name, selected);
            }

            for (var i = 0; i < selected.length; i++) {
                $(this).find('input:checkbox[value=' + selected[i] + ']').prop('checked', true);
            }

            table._updateVisibleColumns(settings, widget);

            $(this).find('input:checkbox').change(function(e) {
                e.stopPropagation();
                table._updateVisibleColumns(settings, widget);
            });
        });

        return $(this);
    };

    $.fn.columner.settings = {
        cookie_name: 'columner'
    };
}(jQuery));
