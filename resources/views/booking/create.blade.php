<div class="ui large modal creating-booking">
    <i class="close icon"></i>
    <div class="header">
        Crea Prenotazione
    </div>
    <div class="scrolling content">
        <form method="POST" action="{{ route('booking.store') }}" class="ui form">
            @csrf

            <input type="hidden" name="selected_room" value="{{ $selected_room }}">
            <input type="hidden" name="selected_day" value="{{ $selected_day }}">
            <input type="hidden" name="selected_start_hour" value="{{ $selected_start_hour }}">
            <input type="hidden" name="selected_end_hour" value="{{ $selected_end_hour }}">

            <div class="wizard">
                <div class="ui three top attached steps">
                    <div class="active step">
                        <div class="content">
                            <div class="title">Chi</div>
                        </div>
                    </div>
                    <div class="step">
                        <div class="content">
                            <div class="title">Quando</div>
                        </div>
                    </div>
                    <div class="step">
                        <div class="content">
                            <div class="title">Quanto</div>
                        </div>
                    </div>
                </div>

                <div class="ui attached segment">
                    <div class="wizard-step active" id="first_step_booking">
                        @if($grant == null)
                            <div class="field">
                                <label>Cliente</label>
                                @include('booking.customers.search', ['can_create_on' => 'first_step_booking'])
                            </div>
                        @else
                            <div class="field ui search customer_complete">
                                <label>Cliente</label>
                                <p>{{ $grant->customer->name }}</p>
                            </div>

                            @include('grant.embed_ro', ['grant' => $grant])
                        @endif
                    </div>

                    <div class="wizard-step">
                        <div class="three fields">
                            <div class="field">
                                <label>Descrizione</label>
                                <input type="text" name="description" value="">
                            </div>

                            <div class="field">
                                <label>Colore</label>
                                <input type="text" class="color" name="color" value="red">
                            </div>

                            <div class="required field">
                                <label>Tipo</label>
                                <div class="inline fields radio-loader" data-load-url="{{ route('booking.wizard', ['page' => 'spaces']) }}" data-load-area=".booking_spaces">
                                    <div class="field">
                                        <div class="ui radio checkbox">
                                            <input type="radio" name="type" value="recurring" class="hidden" required>
                                            <label>Ricorrente</label>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="ui radio checkbox checked">
                                            <input type="radio" name="type" value="occasional" class="hidden" checked required>
                                            <label>Occasionale</label>
                                        </div>
                                    </div>
                                    <div class="field">
                                        <div class="ui radio checkbox">
                                            <input type="radio" name="type" value="manual" class="hidden" required>
                                            <label>Manuale</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <br>

                        <div class="booking_spaces">
                            @include('booking.spaces.single', compact('selected_room', 'selected_equipment', 'selected_day', 'selected_start_hour', 'selected_end_hour'))
                        </div>
                    </div>

                    <div class="wizard-step payments-wrapper">
                        <div class="field">
                            <label>Numero Partecipanti</label>
                            <input type="number" name="partecipants" value="0">
                        </div>

                        <div class="field">
                            <label>Pagamento</label>
                            <div class="grouped fields radio-loader" data-load-url="{{ route('booking.wizard', ['page' => 'payments']) }}" data-load-area=".booking_payments">
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="payment" value="pay" class="hidden">
                                        <label>A Pagamento</label>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="payment" value="free" class="hidden" checked>
                                        <label>Gratuito</label>
                                    </div>
                                </div>
                                <div class="field">
                                    <div class="ui radio checkbox">
                                        <input type="radio" name="payment" value="contribute" class="hidden">
                                        <label>Contributo</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="booking_payments">
                            @include('booking.payments.free')
                        </div>

                        @include('booking.payments.deposit', ['booking' => null])
                    </div>
                </div>

                <div class="ui basic right floated segment wizard-actions">
                    <button class="ui button wizard-prev">Indietro</button>
                    <button class="ui button wizard-next">Avanti</button>
                    <button class="ui green button wizard-finish">Salva</button>
                </div>
            </div>
        </form>
    </div>
</div>
