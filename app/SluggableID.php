<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

trait SluggableID
{
    public static function tFind($id)
    {
        $class = get_called_class();

        if (in_array('Illuminate\Database\Eloquent\SoftDeletes', class_uses($class)))
            return $class::where('id', $id)->withoutGlobalScopes()->withTrashed()->first();
        else
            return $class::find($id);
    }

    /*
        Sovrascrivere questa funzione per usare un parametro diverso dal nome
        per computare l'ID dell'oggetto
    */
    public function getSluggableID()
    {
        return $this->name;
    }

    public function getSlugID()
    {
        $append = '';
        $index = 1;
        $classname = get_class($this);

        while(true) {
            $slug = str_replace('-', '', Str::slug($this->getSluggableID())) . $append;
            if ($classname::find($slug) != null) {
                $append = '_' . $index;
                $index++;
            }
            else {
                break;
            }
        }

        return $slug;
    }
}
