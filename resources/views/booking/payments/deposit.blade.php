<h4 class="ui horizontal divider header">
    <i class="money bill alternate icon"></i>
    Cauzione
</h4>

<div class="two fields">
    <div class="field">
        <label>Cauzione</label>
        <div class="ui right labeled input">
            <input type="number" step="0.01" name="deposit" value="{{ $booking ? $booking->deposit : '0' }}">
            <div class="ui basic label">€</div>
        </div>
    </div>
    <div class="field">
        <label>Stato Cauzione</label>
        <select name="deposit_status">
            <option value="to_pay" {{ $booking && $booking->deposit_status == 'to_pay' ? 'selected' : '' }}>Da Versare</option>
            <option value="payed" {{ $booking && $booking->deposit_status == 'payed' ? 'selected' : '' }}>Versata</option>
            <option value="returned" {{ $booking && $booking->deposit_status == 'returned' ? 'selected' : '' }}>Restituita</option>
        </select>
    </div>
</div>
